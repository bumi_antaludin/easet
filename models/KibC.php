<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kib_c".
 *
 * @property integer $id
 * @property string $kd_brg
 * @property string $kd_lks
 * @property string $kd_skpd
 * @property string $tgl_catat
 * @property string $tgl_oleh
 * @property integer $nillai_kap
 * @property integer $nilai_kon
 * @property integer $nilai_aset
 * @property integer $nilai_retensi
 * @property string $no_reg
 * @property string $kd_rek
 * @property integer $jml
 * @property string $no_spk
 * @property string $asal
 * @property string $no_doc
 * @property string $tgl_doc
 * @property string $kd_tnh
 * @property string $luas_tnh
 * @property string $stts_tnh
 * @property string $luas_gdg
 * @property string $konstruksi
 * @property string $beton
 * @property string $luas_lantai
 * @property string $kondisi
 * @property string $letak
 * @property string $ket
 */
class KibC extends \yii\db\ActiveRecord
{
    public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	public $kd_milik;
	public $berdasarkan;
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kib_c';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tgl_catat', 'tgl_oleh', 'tgl_doc'], 'safe'],
            [['nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml'], 'integer'],
            [['luas_tnh', 'luas_gdg', 'luas_lantai'], 'number'],
            [['kd_brg', 'no_reg', 'kd_rek', 'konstruksi', 'beton', 'kondisi', 'letak'], 'string', 'max' => 50],
            [['kd_lks', 'kd_skpd'], 'string', 'max' => 22],
            [['no_spk', 'asal', 'no_doc', 'kd_tnh', 'stts_tnh'], 'string', 'max' => 100],
            [['ket'], 'string', 'max' => 200],
            [['kd_brg'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_brg' => 'Kode Barang',
            'kd_lks' => 'Kode Lokasi',
            'kd_skpd' => 'Kode SKPD',
            'tgl_catat' => 'Tanggal Catat',
            'tgl_oleh' => 'Tanggal Peroleh',
            'nillai_kap' => 'Nilai Kapitalisasi',
            'nilai_kon' => 'Nilai Kontrak',
            'nilai_aset' => 'Nilai Aset',
            'nilai_retensi' => 'Nilai Retensi',
            'no_reg' => 'No Registrasi',
            'kd_rek' => 'Kd Rekening',
            'jml' => 'Jumlah',
            'no_spk' => 'No SPK',
            'asal' => 'Asal',
            'no_doc' => 'No Dokumen',
            'tgl_doc' => 'Tanggal Dokumen',
            'kd_tnh' => 'Kode Tanah',
            'luas_tnh' => 'Luas Tanah',
            'stts_tnh' => 'Status Tanah',
            'luas_gdg' => 'Luas Gedung',
            'konstruksi' => 'Konstruksi',
            'beton' => 'Beton',
            'luas_lantai' => 'Luas Lantai',
            'kondisi' => 'Kondisi',
            'letak' => 'Letak',
            'ket' => 'Keterangan',
			'thn' => 'Tahun',
			'kd_milik' => 'Kode Pemilik',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
