<?php

namespace app\models;

use Yii;
use yii\web\IdentityInterface;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;


/**
 * This is the model class for table "user".
 *
 * @property integer $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */

class User extends ActiveRecord implements IdentityInterface
{

    public $password;

const STATUS_DELETED = 0;
const STATUS_ACTIVE = 10;

public static function tableName(){
    return 'user';
}

public function behavior()
{
    return [
    TimestampBehavior::className(),
    ];
}

    
    public function rules() {
        
        return [
            ['username','filter','filter'=>'trim'],
            ['username','required'],
            ['username','unique','targetClass'=> '\app\models\User',
                'message'=>'This username has already been taken.'
                ],
            ['username','string','min'=>2,'max'=>255],
            ['email','filter','filter'=>'trim'],
            ['email','required'],
            ['email','email'],
            ['email','string','min'=>6,'max'=>255],
            ['email','unique','targetClass'=>'\app\models\User',
                'message'=>'This email address has already been taken.'],
            ['password','required'],
            ['password','string','min'=>6],
        
    
        ['status','default','value'=> self::STATUS_ACTIVE],
        ['status','in','range'=> [self::STATUS_ACTIVE,self::STATUS_DELETED]],
    ];
}

/**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
return static::findOne(['id'=>$id,'status'=>  self::STATUS_ACTIVE]);      
//  return isset(self::$users[$id]) ? new static(self::$users[$id]) : null;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
/*        foreach (self::$users as $user) {
            if ($user['accessToken'] === $token) {
                return new static($user);
            }
        }

        return null;
 * 
 */
    }

    /**
     * Finds user by username
     *
     * @param  string      $username
     * @return static|null
     */
    public static function findByUsername($username)
    {
            return static::findOne(['username'=>$username , 'status'=>  self::STATUS_ACTIVE]);
    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
  return $this->getPrimaryKey();
        //     return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
//        return $this->authKey;
    }

    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

 public function setPassword($password)
 {
     $this->password_hash = Yii::$app->security->generatePasswordHash($password);
 }
 
 public function generateAuthKey()
 {
     $this->auth_key = Yii::$app->security->generateRandomString();
 }
 
 /**
     * Validates password
     *
     * @param  string  $password password to validate
     * @return boolean if password provided is valid for current user
     */
    public function validatePassword($password)
    {
return Yii::$app->security->validatePassword($password, $this->password_hash);
//        return $this->password === $password;
    }
    
    public function generatePasswordResetToken(){
        $this->password_reset_token = Yii::$app->security->generateRandomKey() . '_' . time();
    }
    
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }
}
