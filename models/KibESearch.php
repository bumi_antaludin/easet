<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KibE;

/**
 * KibESearch represents the model behind the search form about `app\models\KibE`.
 */
class KibESearch extends KibE
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml'], 'integer'],
            [['kd_brg', 'kd_lks', 'kd_skpd', 'tgl_catat', 'tgl_oleh', 'no_reg', 'kd_rek', 'no_spk', 'asal', 'buku_jdl', 'buku_spek', 'brg_asal', 'brg_cipta', 'brg_bhn', 'hw_jns', 'hw_ukuran', 'merk', 'tipe', 'no_seri', 'ukuran', 'bahan', 'kondisi', 'ket'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KibE::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_catat' => $this->tgl_catat,
            'tgl_oleh' => $this->tgl_oleh,
            'nillai_kap' => $this->nillai_kap,
            'nilai_kon' => $this->nilai_kon,
            'nilai_aset' => $this->nilai_aset,
            'nilai_retensi' => $this->nilai_retensi,
            'jml' => $this->jml,
        ]);

        $query->andFilterWhere(['like', 'kd_brg', $this->kd_brg])
            ->andFilterWhere(['like', 'kd_lks', $this->kd_lks])
            ->andFilterWhere(['like', 'kd_skpd', $this->kd_skpd])
            ->andFilterWhere(['like', 'no_reg', $this->no_reg])
            ->andFilterWhere(['like', 'kd_rek', $this->kd_rek])
            ->andFilterWhere(['like', 'no_spk', $this->no_spk])
            ->andFilterWhere(['like', 'asal', $this->asal])
            ->andFilterWhere(['like', 'buku_jdl', $this->buku_jdl])
            ->andFilterWhere(['like', 'buku_spek', $this->buku_spek])
            ->andFilterWhere(['like', 'brg_asal', $this->brg_asal])
            ->andFilterWhere(['like', 'brg_cipta', $this->brg_cipta])
            ->andFilterWhere(['like', 'brg_bhn', $this->brg_bhn])
            ->andFilterWhere(['like', 'hw_jns', $this->hw_jns])
            ->andFilterWhere(['like', 'hw_ukuran', $this->hw_ukuran])
            ->andFilterWhere(['like', 'merk', $this->merk])
            ->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'no_seri', $this->no_seri])
            ->andFilterWhere(['like', 'ukuran', $this->ukuran])
            ->andFilterWhere(['like', 'bahan', $this->bahan])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'ket', $this->ket]);

        return $dataProvider;
    }
}
