<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "rkpbu".
 *
 * @property integer $id
 * @property integer $kd_skp
 * @property integer $kd_brg
 * @property integer $kd_lks
 * @property integer $jml
 * @property integer $hrg
 * @property integer $jml_biaya
 * @property integer $kd_rek
 * @property string $ket
 */
class Rkpbu extends \yii\db\ActiveRecord
{
	
	public $jml;
	public $satuan;
	public $hrg;
	public $ttl;
	public $jml2;
	public $hrg2;
	public $ttl2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rkpbu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_rkpbu'], 'required'],
            [['kd_rkpbu', 'kd_skpd', 'kd_sub2', 'kd_lks', 'kd_rek'], 'integer'],
            [['thn'], 'safe'],
            [['nm_brg', 'ket', 'spek', 'sts_pelihara'], 'string', 'max' => 50],
            [['kd_rkpbu'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_rkpbu' => 'Kode RKPBU',
            'kd_skpd' => 'Kode SKPD',
            'kd_sub2' => 'Kode Sub-Sub Kelompok Barang',
            'kd_lks' => 'Kode Lokasi',
            'thn' => 'Tahun',
            'nm_brg' => 'Nama Barang',
            'ket' => 'Keterangan',
            'spek' => 'Spesifikasi',
            'sts_pelihara' => 'Status Pelihara',
            'kd_rek' => 'Kode Rekening',
			'thn' => 'Tahun',
			'jml' => 'Jumlah',
			'hrg' => 'Harga',
			'ttl' => 'Total',
			'jml2' => 'Jumlah',
			'hrg2' => 'Harga',
			'ttl2' => 'Total',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
