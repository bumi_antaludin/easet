<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kib_a".
 *
 * @property integer $id
 * @property string $jns_brg
 * @property string $kd_brg
 * @property string $no_reg
 * @property string $luas
 * @property string $thn
 * @property string $letak
 * @property string $hak
 * @property string $tgl_ser
 * @property string $no_ser
 * @property string $guna
 * @property string $asal
 * @property string $harga
 * @property string $ket
 */
class KibA extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kib_a';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['luas'], 'number'],
            [['tgl_ser'], 'safe'],
            [['jns_brg', 'kd_brg', 'kd_lks', 'no_reg', 'thn', 'hak', 'no_ser', 'guna', 'harga'], 'string', 'max' => 50],
            [['letak', 'ket'], 'string', 'max' => 200],
            [['asal'], 'string', 'max' => 100],
            [['kd_brg'], 'unique'],
            [['no_reg'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'jns_brg' => 'Jns Brg',
            'kd_brg' => 'Kd Brg',
            'kd_lks' => 'Kd Lks',
            'no_reg' => 'No Reg',
            'luas' => 'Luas',
            'thn' => 'Thn',
            'letak' => 'Letak',
            'hak' => 'Hak',
            'tgl_ser' => 'Tgl Ser',
            'no_ser' => 'No Ser',
            'guna' => 'Guna',
            'asal' => 'Asal',
            'harga' => 'Harga',
            'ket' => 'Ket',
        ];
    }
}
