<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "pengadaan".
 *
 * @property integer $id
 * @property integer $kd_ada
 * @property integer $kd_brghbs
 * @property integer $jml
 * @property integer $hrg
 * @property integer $ttl
 * @property string $ket
 */
class Pengadaan extends \yii\db\ActiveRecord
{
    public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'brg_hbs';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_ada', 'kd_brghbs', 'jml', 'hrg', 'ttl'], 'integer'],
            [['ket'], 'string', 'max' => 100],
            [['kd_ada'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_ada' => 'Kode Pengadaan',
            'kd_brghbs' => 'Kode Barang Habis',
            'jml' => 'Jumlah',
            'hrg' => 'Harga',
            'ttl' => 'Total',
            'ket' => 'Keterangan',
			'jns_brg' => 'Jenis Barang',
        ];
    }
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
