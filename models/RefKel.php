<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_kel".
 *
 * @property integer $id
 * @property string $kd_bid
 * @property string $kd_kel
 * @property string $uraian
 */
class RefKel extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_kel';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_bid'], 'string', 'max' => 5],
            [['kd_kel'], 'string', 'max' => 2],
            [['uraian'], 'string', 'max' => 99],
       //     [['kd_kel'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_bid' => 'Kode Bidang',
            'kd_kel' => 'Kode Kelompok',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function Getkel(){
        
        $kel = RefKel::find()
                ->select(['kd_kel','uraian'])
                ->all();
        
        return \yii\helpers\ArrayHelper::map($kel, 'kd_kel', 'uraian');
    }
}
