<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_gol".
 *
 * @property integer $id
 * @property string $kd_gol
 * @property string $uraian
 */
class RefGol extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_gol';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_gol'], 'string', 'max' => 2],
            [['uraian'], 'string', 'max' => 99],
            [['kd_gol'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_gol' => 'Kode Golongan',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function getGol(){
        $Gol = RefGol::find()
                ->select('kd_gol,uraian')
                ->indexBy('kd_gol')
                ->all();
       return \yii\helpers\ArrayHelper::map($Gol, 'kd_gol', 'uraian');
    }
}
