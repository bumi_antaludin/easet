<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "kib_e".
 *
 * @property integer $id
 * @property string $kd_brg
 * @property string $kd_lks
 * @property string $kd_skpd
 * @property string $tgl_catat
 * @property string $tgl_oleh
 * @property integer $nillai_kap
 * @property integer $nilai_kon
 * @property integer $nilai_aset
 * @property integer $nilai_retensi
 * @property string $no_reg
 * @property string $kd_rek
 * @property integer $jml
 * @property string $no_spk
 * @property string $asal
 * @property string $buku_jdl
 * @property string $buku_spek
 * @property string $brg_asal
 * @property string $brg_cipta
 * @property string $brg_bhn
 * @property string $hw_jns
 * @property string $hw_ukuran
 * @property string $merk
 * @property string $tipe
 * @property string $no_seri
 * @property string $ukuran
 * @property string $bahan
 * @property string $kondisi
 * @property string $ket
 */
class KibE extends \yii\db\ActiveRecord
{
    public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	public $kd_milik;
	public $berdasarkan;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kib_e';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tgl_catat', 'tgl_oleh'], 'safe'],
            [['nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml'], 'integer'],
            [['kd_brg', 'no_reg', 'kd_rek', 'buku_jdl', 'brg_asal', 'brg_cipta', 'brg_bhn', 'hw_jns', 'hw_ukuran', 'merk', 'tipe', 'no_seri', 'ukuran', 'bahan', 'kondisi'], 'string', 'max' => 50],
            [['kd_lks', 'kd_skpd'], 'string', 'max' => 22],
            [['no_spk', 'asal', 'buku_spek'], 'string', 'max' => 100],
            [['ket'], 'string', 'max' => 200],
            [['kd_brg'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_brg' => 'Kode Barang',
            'kd_lks' => 'Kode Lokasi',
            'kd_skpd' => 'Kode SKPD',
            'tgl_catat' => 'Tanggal Catat',
            'tgl_oleh' => 'Tanggal Peroleh',
            'nillai_kap' => 'Nilai Kapitalisasi',
            'nilai_kon' => 'Nilai Kontrak',
            'nilai_aset' => 'Nilai Aset',
            'nilai_retensi' => 'Nilai Retensi',
            'no_reg' => 'No Registrasi',
            'kd_rek' => 'Kode Rekening',
            'jml' => 'Jumlah',
            'no_spk' => 'No SPK',
            'asal' => 'Asal',
            'buku_jdl' => 'Judul Buku',
            'buku_spek' => 'Spesifikasi Buku',
            'brg_asal' => 'Asal Barang Kesenian',
            'brg_cipta' => 'Pencipta',
            'brg_bhn' => 'Bahan Barang',
            'hw_jns' => 'Jenis Hewan / Tumbuhan',
            'hw_ukuran' => 'Ukuran Hewan / Tumbuhan',
            'merk' => 'Merek Barang',
            'tipe' => 'Tipe Barang',
            'no_seri' => 'No Seri',
            'ukuran' => 'Ukuran',
            'bahan' => 'Bahan',
            'kondisi' => 'Kondisi',
            'ket' => 'Keterangan',
			'thn' => 'Tahun',
			'kd_milik' => 'Kode Pemilik',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
