<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KibC;

/**
 * KibCSearch represents the model behind the search form about `app\models\KibC`.
 */
class KibCSearch extends KibC
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml'], 'integer'],
            [['kd_brg', 'kd_lks', 'kd_skpd', 'tgl_catat', 'tgl_oleh', 'no_reg', 'kd_rek', 'no_spk', 'asal', 'no_doc', 'tgl_doc', 'kd_tnh', 'stts_tnh', 'konstruksi', 'beton', 'kondisi', 'letak', 'ket'], 'safe'],
            [['luas_tnh', 'luas_gdg', 'luas_lantai'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KibC::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_catat' => $this->tgl_catat,
            'tgl_oleh' => $this->tgl_oleh,
            'nillai_kap' => $this->nillai_kap,
            'nilai_kon' => $this->nilai_kon,
            'nilai_aset' => $this->nilai_aset,
            'nilai_retensi' => $this->nilai_retensi,
            'jml' => $this->jml,
            'tgl_doc' => $this->tgl_doc,
            'luas_tnh' => $this->luas_tnh,
            'luas_gdg' => $this->luas_gdg,
            'luas_lantai' => $this->luas_lantai,
        ]);

        $query->andFilterWhere(['like', 'kd_brg', $this->kd_brg])
            ->andFilterWhere(['like', 'kd_lks', $this->kd_lks])
            ->andFilterWhere(['like', 'kd_skpd', $this->kd_skpd])
            ->andFilterWhere(['like', 'no_reg', $this->no_reg])
            ->andFilterWhere(['like', 'kd_rek', $this->kd_rek])
            ->andFilterWhere(['like', 'no_spk', $this->no_spk])
            ->andFilterWhere(['like', 'asal', $this->asal])
            ->andFilterWhere(['like', 'no_doc', $this->no_doc])
            ->andFilterWhere(['like', 'kd_tnh', $this->kd_tnh])
            ->andFilterWhere(['like', 'stts_tnh', $this->stts_tnh])
            ->andFilterWhere(['like', 'konstruksi', $this->konstruksi])
            ->andFilterWhere(['like', 'beton', $this->beton])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'letak', $this->letak])
            ->andFilterWhere(['like', 'ket', $this->ket]);

        return $dataProvider;
    }
}
