<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_gudang".
 *
 * @property integer $id
 * @property integer $kd_gudang
 * @property string $uraian
 */
class RefGudang extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_gudang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_gudang'], 'integer'],
            [['uraian'], 'string', 'max' => 99],
            [['kd_gudang'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_gudang' => 'Kd Gudang',
            'uraian' => 'Uraian',
        ];
    }
}
