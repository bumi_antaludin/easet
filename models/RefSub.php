<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_sub".
 *
 * @property integer $id
 * @property string $kd_kel
 * @property string $kd_sub
 * @property string $uraian
 */
class RefSub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_sub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_kel'], 'string', 'max' => 8],
            [['kd_sub'], 'string', 'max' => 2],
            [['uraian'], 'string', 'max' => 99],
            [['kd_sub'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_kel' => 'Kode Kelompok',
            'kd_sub' => 'Kode Sub Kelompok',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function Getsub()
    {
        $sub = RefSub::find()
                ->select(['kd_sub','uraian'])
                ->all();
        return \yii\helpers\ArrayHelper::map($sub,'kd_sub','uraian');
    }
}
