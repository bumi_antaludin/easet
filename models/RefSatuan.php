<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_satuan".
 *
 * @property integer $id
 * @property integer $kd_sat
 * @property string $uraian
 */
class RefSatuan extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_satuan';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_sat'], 'integer'],
            [['uraian'], 'string', 'max' => 99],
            [['kd_sat'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_sat' => 'Kode Satuan',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function ListSatuan()
    {
        $data = RefSatuan::find()
                ->select(['kd_sat','uraian'])
                ->all();
        
        return \yii\helpers\ArrayHelper::map($data, 'kd_sat', 'uraian');
    }
}
