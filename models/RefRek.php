<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_rek".
 *
 * @property integer $id
 * @property integer $kd_rek
 * @property integer $kd_sub2
 * @property integer $kd_detail
 * @property string $uraian
 */
class RefRek extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_rek';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_rek', 'kd_detail'], 'integer'],
            [['uraian'], 'string', 'max' => 99],
            [['kd_rek'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_rek' => 'Kode Rekening',
            'kd_detail' => 'Kode Detail',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function getrek(){
        $rek = RefRek::find()->asArray()->all();
        
        return \yii\helpers\ArrayHelper::map($rek, 'kd_rek', 'uraian');
    }
}
