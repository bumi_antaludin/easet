<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KibA;

/**
 * KibASearch represents the model behind the search form about `app\models\KibA`.
 */
class KibASearch extends KibA
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'kd_rek', 'jml'], 'integer'],
            [['kd_brg', 'kd_lks', 'kd_skpd', 'tgl_catat', 'tgl_oleh', 'no_reg', 'no_spk', 'asal', 'status', 'no_ser', 'tgl_ser', 'letak', 'guna', 'ket', 'kondisi'], 'safe'],
            [['luas'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KibA::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_catat' => $this->tgl_catat,
            'tgl_oleh' => $this->tgl_oleh,
            'nillai_kap' => $this->nillai_kap,
            'nilai_kon' => $this->nilai_kon,
            'nilai_aset' => $this->nilai_aset,
            'nilai_retensi' => $this->nilai_retensi,
            'kd_rek' => $this->kd_rek,
            'jml' => $this->jml,
            'tgl_ser' => $this->tgl_ser,
            'luas' => $this->luas,
        ]);

        $query->andFilterWhere(['like', 'kd_brg', $this->kd_brg])
            ->andFilterWhere(['like', 'kd_lks', $this->kd_lks])
            ->andFilterWhere(['like', 'kd_skpd', $this->kd_skpd])
            ->andFilterWhere(['like', 'no_reg', $this->no_reg])
            ->andFilterWhere(['like', 'no_spk', $this->no_spk])
            ->andFilterWhere(['like', 'asal', $this->asal])
            ->andFilterWhere(['like', 'status', $this->status])
            ->andFilterWhere(['like', 'no_ser', $this->no_ser])
            ->andFilterWhere(['like', 'letak', $this->letak])
            ->andFilterWhere(['like', 'guna', $this->guna])
            ->andFilterWhere(['like', 'ket', $this->ket])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi]);

        return $dataProvider;
    }
}
