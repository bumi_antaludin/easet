<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_bidang".
 *
 * @property integer $id
 * @property string $kd_gol
 * @property string $kd_bid
 * @property string $uraian
 */
class RefBidang extends \yii\db\ActiveRecord
{
    
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_bidang';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_gol'], 'string', 'max' => 2],
            [['kd_bid'], 'string', 'max' => 4],
            [['uraian'], 'string', 'max' => 99],
     //       [['kd_bid'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_gol' => 'Kode Golongan',
            'kd_bid' => 'Kode Bidang',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function GetBid()
    {
        $bid = RefBidang::find()
                ->select('kd_gol,kd_bid,uraian')
                ->all();
       return \yii\helpers\ArrayHelper::map($bid, 'kd_bid', 'uraian');
                
    }
}
