<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_skpd".
 *
 * @property integer $id
 * @property integer $kd_skpd
 * @property string $uraian
 */
class RefSkpd extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_skpd';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_skpd'], 'integer'],
            [['uraian'], 'string', 'max' => 99],
            [['kd_skpd'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_skpd' => 'Kode Skpd',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function getskpd(){
        $skpd = RefSkpd::find()
                ->select(['kd_skpd','uraian'])
                ->where('tahun="2015" and length(kd_skpd)>7 and left(kd_skpd,1)<>"9"')
                ->asArray()
                ->all();
        
        return \yii\helpers\ArrayHelper::map($skpd, 'kd_skpd', 'uraian');
        
    }
    
    public static function getLokasi()
    {
        $kd_milik= new Pemilik();
        
    }
}
