<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_subsub".
 *
 * @property integer $id
 * @property string $kd_sub
 * @property string $kd_sub2
 * @property string $uraian
 */
class RefSubsub extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_subsub';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_sub'], 'string', 'max' => 11],
            [['kd_sub2'], 'string', 'max' => 2],
            [['uraian'], 'string', 'max' => 99],
      //      [['kd_sub2'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_sub' => 'Kode Sub Kelompok',
            'kd_sub2' => 'Kode Sub-Sub Kelompok',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function Getsubsub()
    {
        $subsub = RefSubsub::find()
                ->select(['kd_sub2','uraian'])
                ->all();
        return \yii\helpers\ArrayHelper::map($subsub, 'kd_sub2', 'uraian');
    }
    
   public static function GetBrgHabis(){
       $data = RefSubsub::find()
               ->select(['kd_sub2','uraian'])
               ->where('length(kd_sub2)>8 and left(kd_sub2,1)="5"')
               ->all();
          
               return \yii\helpers\ArrayHelper::map($data, 'kd_sub2', 'uraian');
   }
}
