<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "rkbu".
 *
 * @property integer $id
 * @property integer $kd_rkbu
 * @property integer $kd_skpd
 * @property integer $kd_sub2
 * @property string $nm_brg
 * @property string $spek
 * @property string $thn
 * @property integer $kd_rek
 */
class Rkbu extends \yii\db\ActiveRecord
{
	
    public $thnx;
    public $jml;
	public $satuan;
	public $hrg;
	public $ttl;
	public $jml2;
	public $hrg2;
	public $ttl2;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'rkbu';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_rkbu'], 'required'],
            [['kd_rkbu', 'kd_sub2', 'kd_lks', 'kd_rek'], 'integer'],
            [['nm_brg', 'kd_skpd','thn', 'ket', 'spek'], 'string', 'max' => 50],
            [['kd_rkbu'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_rkbu' => 'Kode RKBU',
            'kd_skpd' => 'Kode SKPD',
            'kd_sub2' => 'Kode Sub-Sub Kelompok Barang',
            'kd_lks' => 'Kode Lokasi',
            'thn' => 'Tahun',
            'nm_brg' => 'Nama Barang',
            'ket' => 'Keterangan',
            'spek' => 'Spesifikasi',
            'kd_rek' => 'Kode Rekening',
			'jml' => 'Jumlah',
			'hrg' => 'Harga',
			'ttl' => 'Total',
			'jml2' => 'Jumlah',
			'hrg2' => 'Harga',
			'ttl2' => 'Total',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
