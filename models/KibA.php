<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "kib_a".
 *
 * @property integer $id
 * @property string $kd_brg
 * @property string $kd_lks
 * @property string $kd_skpd
 * @property string $tgl_catat
 * @property string $tgl_oleh
 * @property integer $nillai_kap
 * @property integer $nilai_kon
 * @property integer $nilai_aset
 * @property integer $nilai_retensi
 * @property string $no_reg
 * @property integer $kd_rek
 * @property integer $jml
 * @property string $no_spk
 * @property string $asal
 * @property string $status
 * @property string $no_ser
 * @property string $tgl_ser
 * @property string $luas
 * @property string $letak
 * @property string $guna
 * @property string $ket
<<<<<<< HEAD
 * @property string $kondisi
=======
 * @property string $kd_lks 
>>>>>>> 0fbd9e700e2c33e92f1fc39f33964c63232be5b2
 */
class KibA extends \yii\db\ActiveRecord
{
	
	public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	public $kd_milik;
	public $berdasarkan;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kib_a';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tgl_catat', 'tgl_oleh', 'tgl_ser'], 'safe'],
            [['nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml'], 'integer'],
            [['luas'], 'number'],
            [['kd_brg', 'kd_rek', 'no_reg', 'no_ser', 'guna', 'kondisi'], 'string', 'max' => 50],
            [['kd_lks', 'kd_skpd'], 'string', 'max' => 22],
            [['no_spk', 'asal', 'status'], 'string', 'max' => 100],
            [['letak', 'ket'], 'string', 'max' => 200],
            [['kd_brg'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_brg' => 'Kode Barang',
            'kd_lks' => 'Kode Lokasi',
            'kd_skpd' => 'Kode SKPD',
            'tgl_catat' => 'Tanggal Catat',
            'tgl_oleh' => 'Tanggal Peroleh',
            'nillai_kap' => 'Nilai Kapitalisai',
            'nilai_kon' => 'Nilai Kontrak',
            'nilai_aset' => 'Nilai Aset',
            'nilai_retensi' => 'Nilai Retensi',
            'no_reg' => 'No Reg',
            'kd_rek' => 'Kode Rekening',
            'jml' => 'Jumlah',
            'no_spk' => 'No SPK',
            'asal' => 'Asal',
            'status' => 'Status',
            'no_ser' => 'No Sertifikat',
            'tgl_ser' => 'Tanggal Sertifikat',
            'luas' => 'Luas',
            'letak' => 'Letak',
            'guna' => 'Pengguna',
            'ket' => 'Keterangan',
            'kondisi' => 'Kondisi',
			'thn' => 'Tahun',
			'kd_milik' => 'Kode Pemilik',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->asArray()->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
