<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KibA;

/**
 * KibASearch represents the model behind the search form about `app\models\KibA`.
 */
class KibASearch extends KibA
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['jns_brg', 'kd_brg', 'no_reg', 'thn', 'letak', 'hak', 'tgl_ser', 'no_ser', 'guna', 'asal', 'harga', 'ket'], 'safe'],
            [['luas'], 'number'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KibA::find();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->andFilterWhere([
            'id' => $this->id,
            'luas' => $this->luas,
            'tgl_ser' => $this->tgl_ser,
        ]);

        $query->andFilterWhere(['like', 'jns_brg', $this->jns_brg])
            ->andFilterWhere(['like', 'kd_brg', $this->kd_brg])
            ->andFilterWhere(['like', 'no_reg', $this->no_reg])
            ->andFilterWhere(['like', 'thn', $this->thn])
            ->andFilterWhere(['like', 'letak', $this->letak])
            ->andFilterWhere(['like', 'hak', $this->hak])
            ->andFilterWhere(['like', 'no_ser', $this->no_ser])
            ->andFilterWhere(['like', 'guna', $this->guna])
            ->andFilterWhere(['like', 'asal', $this->asal])
            ->andFilterWhere(['like', 'harga', $this->harga])
            ->andFilterWhere(['like', 'ket', $this->ket]);

        return $dataProvider;
    }
}
