<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "kib_b".
 *
 * @property integer $id
 * @property string $kd_brg
 * @property string $kd_lks
 * @property string $kd_skpd
 * @property string $tgl_catat
 * @property string $tgl_oleh
 * @property integer $nillai_kap
 * @property integer $nilai_kon
 * @property integer $nilai_aset
 * @property integer $nilai_retensi
 * @property string $no_reg
 * @property integer $kd_rek
 * @property integer $jml
 * @property string $no_spk
 * @property string $asal
 * @property string $merk
 * @property string $tipe
 * @property string $ukuran
 * @property integer $cilinder
 * @property string $bahan
 * @property string $no_pabrik
 * @property string $no_rangka
 * @property string $no_mesin
 * @property string $no_pol
 * @property string $no_bpkb
 * @property string $kondisi
 * @property string $pegang
 * @property string $ket
 */
class KibB extends \yii\db\ActiveRecord
{
    public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	public $kd_milik;
	public $berdasarkan;
	
	/**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kib_b';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tgl_catat', 'tgl_oleh'], 'safe'],
            [['nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'jml', 'cilinder'], 'integer'],
            [['kd_brg', 'kd_rek', 'no_reg', 'merk', 'tipe', 'ukuran', 'bahan', 'no_pabrik', 'no_rangka', 'no_mesin', 'no_pol', 'no_bpkb', 'kondisi', 'pegang'], 'string', 'max' => 50],
            [['kd_lks', 'kd_skpd'], 'string', 'max' => 22],
            [['no_spk', 'asal'], 'string', 'max' => 100],
            [['ket'], 'string', 'max' => 200],
            [['kd_brg'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_brg' => 'Kode Barang',
            'kd_lks' => 'Kode Lokasi',
            'kd_skpd' => 'Kode SKPD',
            'tgl_catat' => 'Tanggal Catat',
            'tgl_oleh' => 'Tanggal Peroleh',
            'nillai_kap' => 'Nilai Kapitalisasi',
            'nilai_kon' => 'Nilai Kontrak',
            'nilai_aset' => 'Nilai Aset',
            'nilai_retensi' => 'Nilai Retensi',
            'no_reg' => 'No Registrasi',
            'kd_rek' => 'Kode Rekening',
            'jml' => 'Jumlah',
            'no_spk' => 'No SPK',
            'asal' => 'Asal',
            'merk' => 'Merk',
            'tipe' => 'Tipe',
            'ukuran' => 'Ukuran',
            'cilinder' => 'Cilinder',
            'bahan' => 'Bahan',
            'no_pabrik' => 'No Pabrik',
            'no_rangka' => 'No Rangka',
            'no_mesin' => 'No Mesin',
            'no_pol' => 'No Pol',
            'no_bpkb' => 'No BPKB',
            'kondisi' => 'Kondisi',
            'pegang' => 'Pemegang',
            'ket' => 'Keterangan',
			'thn' => 'Tahun',
			'kd_milik' => 'Kode Pemilik',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
