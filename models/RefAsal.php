<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ref_asal".
 *
 * @property integer $id
 * @property string $kd_asal
 * @property string $uraian
 */
class RefAsal extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ref_asal';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_asal'], 'string', 'max' => 50],
            [['uraian'], 'string', 'max' => 99],
            [['kd_asal'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_asal' => 'Kode Asal',
            'uraian' => 'Uraian',
        ];
    }
}
