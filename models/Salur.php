<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
/**
 * This is the model class for table "salur".
 *
 * @property integer $id
 * @property integer $kd_ada
 * @property integer $kd_brghbs
 * @property integer $jml
 * @property integer $hrg
 * @property integer $ttl
 * @property string $ket
 */
class Salur extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
	 
	public $jns_brg;
	public $kd_skpd;
	public $nm_brg;
	public $from_date;
	public $to_date	;
	public $thn	;
	public $sem	;
	
    public static function tableName()
    {
        return 'salur';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_ada', 'kd_brghbs', 'jml', 'hrg', 'ttl'], 'integer'],
            [['ket'], 'string', 'max' => 100],
            [['kd_ada'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_ada' => 'Kode Penyaluran',
            'kd_brghbs' => 'Kode Barag Habis',
            'jml' => 'Jumlah',
            'hrg' => 'Harga',
            'ttl' => 'Total',
            'ket' => 'Keterangan',
			'jns_brg' => 'Jenis Barang',
			'nm_brg' => 'Nama Barang',
			'thn' => 'Tahun',
			'sem' => 'Semester',
        ];
    }
	
	public static function getSkpd()
    {
        $droptions=  Refskpd::findBySql("Select kd_skpd,uraian from ref_skpd where tahun='2015' and length(kd_skpd)>7")->all();
        return ArrayHelper::map($droptions, 'kd_skpd', 'uraian');
    }
}
