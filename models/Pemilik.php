<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "pemilik".
 *
 * @property integer $id
 * @property integer $kd_milik
 * @property string $uraian
 */
class Pemilik extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'pemilik';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['kd_milik'], 'integer'],
            [['uraian'], 'string', 'max' => 99],
            [['kd_milik'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'kd_milik' => 'Kode Pemilik',
            'uraian' => 'Uraian',
        ];
    }
    
    public static function getPemilik()
    {
        $pemilik = Pemilik::find()
                ->select(['kd_milik','uraian'])
                ->all();
        
        return \yii\helpers\ArrayHelper::map($pemilik, 'kd_milik', 'uraian');
        
    }
}
