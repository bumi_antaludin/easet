<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\KibB;

/**
 * KibBSearch represents the model behind the search form about `app\models\KibB`.
 */
class KibBSearch extends KibB
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'nillai_kap', 'nilai_kon', 'nilai_aset', 'nilai_retensi', 'kd_rek', 'jml', 'cilinder'], 'integer'],
            [['kd_brg', 'kd_lks', 'kd_skpd', 'tgl_catat', 'tgl_oleh', 'no_reg', 'no_spk', 'asal', 'merk', 'tipe', 'ukuran', 'bahan', 'no_pabrik', 'no_rangka', 'no_mesin', 'no_pol', 'no_bpkb', 'kondisi', 'pegang', 'ket'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = KibB::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'tgl_catat' => $this->tgl_catat,
            'tgl_oleh' => $this->tgl_oleh,
            'nillai_kap' => $this->nillai_kap,
            'nilai_kon' => $this->nilai_kon,
            'nilai_aset' => $this->nilai_aset,
            'nilai_retensi' => $this->nilai_retensi,
            'kd_rek' => $this->kd_rek,
            'jml' => $this->jml,
            'cilinder' => $this->cilinder,
        ]);

        $query->andFilterWhere(['like', 'kd_brg', $this->kd_brg])
            ->andFilterWhere(['like', 'kd_lks', $this->kd_lks])
            ->andFilterWhere(['like', 'kd_skpd', $this->kd_skpd])
            ->andFilterWhere(['like', 'no_reg', $this->no_reg])
            ->andFilterWhere(['like', 'no_spk', $this->no_spk])
            ->andFilterWhere(['like', 'asal', $this->asal])
            ->andFilterWhere(['like', 'merk', $this->merk])
            ->andFilterWhere(['like', 'tipe', $this->tipe])
            ->andFilterWhere(['like', 'ukuran', $this->ukuran])
            ->andFilterWhere(['like', 'bahan', $this->bahan])
            ->andFilterWhere(['like', 'no_pabrik', $this->no_pabrik])
            ->andFilterWhere(['like', 'no_rangka', $this->no_rangka])
            ->andFilterWhere(['like', 'no_mesin', $this->no_mesin])
            ->andFilterWhere(['like', 'no_pol', $this->no_pol])
            ->andFilterWhere(['like', 'no_bpkb', $this->no_bpkb])
            ->andFilterWhere(['like', 'kondisi', $this->kondisi])
            ->andFilterWhere(['like', 'pegang', $this->pegang])
            ->andFilterWhere(['like', 'ket', $this->ket]);

        return $dataProvider;
    }
}
