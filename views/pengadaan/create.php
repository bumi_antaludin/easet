<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Pengadaan */

$this->title = 'Tambah Pengadaan';
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
