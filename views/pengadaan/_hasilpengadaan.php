
<div class="row">
			<div class="col-md-12">
				<div class="box">
						<div class="box-header with-border">
						  <h3 class="box-title">Daftar Hasil Pengadaan Barang</h3>
						</div>
						<div class="box-body kelebihan">
							<table class="table table-bordered">
							<tbody>
							  <tr>
								<th rowspan="2" scope="col">No.</th>
								<th rowspan="2" scope="col">Jenis Barang</th>
								<th colspan="2" scope="col">SPK/Perjanjian/Kontrak</th>
								<th colspan="2" scope="col">DPA/SPM/Kwitansi</th>
								<th colspan="3" scope="col">Jumlah</th>
								<th rowspan="2" scope="col">Dipergunakan<br>
								Untuk</th>
								<th rowspan="2" scope="col">Ket.</th>
							  </tr>
							  <tr>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>Tanggal</th>
								<th>Nomor</th>
								<th>Banyaknya</th>
								<th>Harga Satuan</th>
								<th>Jumlah Harga</th>
							  </tr>
							  <tr align="center">
								<td>1</td>
								<td>2</td>
								<td>3</td>
								<td>4</td>
								<td>5</td>
								<td>6</td>
								<td>7</td>
								<td>8</td>
								<td>9</td>
								<td>10</td>
								<td>11</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							</tbody>
							</table>
						</div>
				</div>
			</div>
	</div>