<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Pengadaan';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="pengadaan-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Dokumen Pengadaan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'kd_ada',
            'kd_brghbs',
            'jml',
            'hrg',
            // 'ttl',
            // 'ket',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
