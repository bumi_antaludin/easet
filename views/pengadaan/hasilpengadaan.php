<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Pengadaan;

?>

<div class="ada-kartu">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-6 ">
			<?php
				echo '<label class="control-label">Pilih Tanggal</label>';
				echo DatePicker::widget([
					'model' => $model,
					'attribute' => 'from_date',
					'attribute2' => 'to_date',
					'options' => ['placeholder' => 'Tanggal Awal'],
					'options2' => ['placeholder' => 'Tanggal Akhir'],
					'type' => DatePicker::TYPE_RANGE,
					'form' => $form,
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'autoclose' => true,
					]
				]);
			?>
		</div>
		<div class="col-md-6 ">
        <label>SKPD</label>
		<?= Select2::widget([
				'name' => 'kd_skpd',
				'data' => app\models\RefSkpd::getskpd(),
				'options' => [
					'placeholder' => 'Pilih SKPD ...',
					'multiple' => false,
					],
                    'pluginOptions' => [
						'allowClear' => true
					],
				]
				); ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
			<?= Html::a('<i class="glyphicon glyphicon-print "></i> Cetak', ['topdf'], ['class' => 'btn btn-success' ,'target'=>'_blank']) ?>

		</div>
	</div>
	<div class="jarak15"></div>
	<?= $this->render('_hasilpengadaan', [
        //'model' => $model,
    ]) ?>
	<?php ActiveForm::end(); ?>
</div>
