<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Pengadaan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="pengadaan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_ada')->textInput() ?>

    <?php 
	$data = ["kd_brghbs" => "01",
			"kd_brghbsX" => "02"
	];
	?>
	<?php echo $form->field($model, 'kd_brghbs')->widget(Select2::classname(), [
    'data' => $data,
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Barang Habis ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'jml')->textInput() ?>

    <?= $form->field($model, 'hrg')->textInput() ?>

    <?= $form->field($model, 'ttl')->textInput() ?>

    <?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
