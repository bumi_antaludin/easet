<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pengadaan */

$this->title = 'Ubah Pengadaan: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pengadaan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="pengadaan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
