<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibC */

$this->title = 'Ubah KIB C: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB C', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-c-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
