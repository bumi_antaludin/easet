
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) C - Gedung dan Bangunan</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						  <tr>
							<th rowspan="2" scope="col">No</th>
							<th rowspan="2" scope="col">Jenis Barang/<br>
							Nama Barang</th>
							<th colspan="2" scope="col">Nomor</th>
							<th rowspan="2" scope="col">Kondisi Bangunan<br>
							(B,KB,RB)</th>
							<th colspan="2" scope="col">Konstruksi</th>
							<th rowspan="2" scope="col">Luas <br>
							  Lantai<br>
							(m2)</th>
							<th rowspan="2" scope="col">Letak/Lokasi<br>
							Alamat</th>
							<th colspan="2" scope="col">Dokumen Gedung</th>
							<th rowspan="2" scope="col">Luas<br>
							(m2)</th>
							<th rowspan="2" scope="col">Status Tanah<br>
							(Hak)</th>
							<th rowspan="2" scope="col">Nomor Kode<br>
							Tanah</th>
							<th rowspan="2" scope="col">Asal<br>
							  Usul</th>
							<th rowspan="2" scope="col">Harga</th>
							<th rowspan="2" scope="col">Keterangan</th>
						  </tr>
						  <tr>
							<th>Kode Barang</th>
							<th>Registrasi</th>
							<th>Tingkat/<br>
							Tidak</th>
							<th>Beton/<br>
							  Tidak</th>
							<th>Tanggal<br>Pengadaan</th>
							<th>Nomor</th>
						  </tr>
						  <tr align="center">
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
							<td>9</td>
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
							<td>15</td>
							<td>16</td>
							<td>17</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>

