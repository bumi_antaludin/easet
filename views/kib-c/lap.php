<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\KibC;
?>
<div class="salur-kartu lap">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 ">
			<?= $form->field($model, 'thn')->dropDownList(["2015" => "2015", "2016" => "2016"]) ?>
		</div>
		<div class="col-md-3 ">
			<?= $form->field($model, 'kd_milik')->dropDownList(
 \app\models\Pemilik::getPemilik()
                                ) ?>
		</div>
		<div class="col-md-3 ">
			<?php echo $form->field($model, 'kd_skpd')->widget(Select2::classname(), [
				'data' => \app\models\RefSkpd::getskpd(),
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
		</div>
		<div class="col-md-3 ">
			<?= $form->field($model, 'berdasarkan')->dropDownList(["Asal Usul" => "Asal Usul", "Hak" => "Hak", "Letak" => "Letak", "Harga" => "Harga"]) ?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
	<?= Html::a('<i class="gylphicon glyphicon-print"></i> Cetak', ['lapkartukibc'], ['class'=>'btn btn-success','target'=>'_blank']) ?>

		</div>
	</div>
	<div class="jarak15"></div>
	
	<?= $this->render('_lap', [
     //   'model' => $model,
    ]) ?>
	
	<?php ActiveForm::end(); ?>
</div>
