<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibC */

$this->title = 'Tambah KIB C';
$this->params['breadcrumbs'][] = ['label' => 'KIB C', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-c-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
