<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefSatuan */

$this->title = 'Tambah Satuan';
$this->params['breadcrumbs'][] = ['label' => 'Satuan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-satuan-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
