<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefSatuan */

$this->title = 'Ubah Satuan Barang: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ubah Satuan', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-satuan-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
