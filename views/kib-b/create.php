<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibB */

$this->title = 'Tambah KIB B';
$this->params['breadcrumbs'][] = ['label' => 'KIB B', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-b-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
