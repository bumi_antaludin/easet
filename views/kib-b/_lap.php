
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) B - Peralatan dan Mesin</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						  <tr>
							<th rowspan="2">No. Urut</th>
							<th rowspan="2">Kode Barang</th>
							<th rowspan="2">Nama Barang /<br>
							Jenis Barang</th>
							<th rowspan="2">Nomor Register</th>
							<th rowspan="2">Merk/<br>
							type</th>
							<th rowspan="2">Ukuran/<br>
							CC</th>
							<th rowspan="2">Bahan</th>
							<th rowspan="2">Tahun Catat/<br>
						Beli</th>
							<th colspan="5">Nomor</th>
							<th rowspan="2">Asal Usul</th>
							<th rowspan="2">Harga</th>
							<th rowspan="2">Keterangan</th>
						  </tr>
						  <tr>
							<th>Pabrik</th>
							<th>Rangka</th>
							<th>Mesin</th>
							<th>Polisi</th>
							<th>BPKB</th>
						  </tr>
						  <tr align="center">
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
							<td>9</td>
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
							<td>15</td>
							<td>16</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						   </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
