<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibB */

$this->title = 'Ubah KIB B: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB B', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-b-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
