<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rkbu */

$this->title = 'Ubah RKBU: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'RKBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="rkbu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
