<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\grid\GridView;
use app\models\Rkbu;
use kartik\select2\Select2;
use yii\widgets\ActiveForm;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'RKBU';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkbu-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah RKBU', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
		
		<?php Pjax::begin(); ?>
		<?= Html::beginForm(['#'], 'post', ['data-pjax' => '', 'class'=>'form-inline']); ?>
		<table style="border: none;">  
			<tr>
				<td>Tahun</td>
				<td>SKPD</td>
				<td>Jenis Barang</td>
				<td></td>
			</tr>
			<tr>
				<td>
					<div class="form-group">
				    <?= Html::input('text', 'string', Yii::$app->request->post('thnx'), [
                                        'class' => 'form-control','placeholder' => 'Tahun',
                                        ]) ?>
					</div>
				</td>
				<td>
					<div class="form-group" style="width: 700px"> 
						<?= Select2::widget([
						'name' => 'kd_skpd',
						'data' => app\models\RefSkpd::getskpd(),
						'options' => [
							'placeholder' => 'Pilih SKPD ...',
							'multiple' => false,
							],
								'pluginOptions' => [
								'allowClear' => true
							],
							]
						); ?>
					</div>
				</td>
				<td>
					<div class="form-group">
					<?= Html::dropDownList('Jenis Barang','jns_brg',['Semua','Barang Inventaris', 'Barang Inventaris'], ['class' => 'form-control','placeholder' => 'Pilih Jenis Barang ...',]) ?>	
					</div>
				</td>
                <td><?= Html::submitButton('Cari', ['class' => 'btn btn-sm btn-primary', 'name' => 'hash-button']) ?></td>
			</tr>
		</table>
	
	<?= Html::endForm() ?>
    <br>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'nm_brg', 'format' => 'text', 'label' => 'Nama Barang'],
			['attribute' => 'jmlrkb', 'format' => 'text', 'label' => 'Jumlah RKB'],
			['attribute' => 'jmldkb', 'format' => 'text', 'label' => 'Jumlah DKP'],
			['attribute' => 'satuan', 'format' => 'text', 'label' => 'Satuan'],
			['attribute' => 'hrgrkb', 'format' => 'text', 'label' => 'Harga RKB'],
			['attribute' => 'hrgdkp', 'format' => 'text', 'label' => 'Harga DKP'],
			['attribute' => 'ttlrkp', 'format' => 'text', 'label' => 'Tanggal RKB'],
			['attribute' => 'ttldkp', 'format' => 'text', 'label' => 'Tanggal DKP'],
			['attribute' => 'kd_rek', 'format' => 'text', 'label' => 'Kode Rekening'],
			['attribute' => 'ket', 'format' => 'text', 'label' => 'Keterangan'],
            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
	</div>
</div>
<?php Pjax::end(); ?>