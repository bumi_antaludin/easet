<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rkbu */

$this->title = 'Tambah RKBU';
$this->params['breadcrumbs'][] = ['label' => 'RKBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkbu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
