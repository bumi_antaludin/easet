<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rkbu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rkbu-form">

    <?php $form = ActiveForm::begin(); ?>

	<?php echo $form->field($model, 'kd_skpd')->widget(Select2::classname(), [
    'data' => app\models\RefSkpd::getskpd(),
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

 
	<?php echo $form->field($model, 'kd_sub2')->widget(Select2::classname(), [
    'data' => \app\models\RefSubsub::Getsubsub(),
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Sub-Sub ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'nm_brg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spek')->textInput(['maxlength' => true]) ?>

    
	
	<div class="row">
		<div class="col-md-6">
		<h3>Rencana Kebutuhan Barang</h3>
                <?= $form->field($model, 'jml')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
		<?= $form->field($model, 'hrg')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
		<?= $form->field($model, 'satuan')->dropDownList(app\models\RefSatuan::ListSatuan())
?>
		<?= $form->field($model, 'ttl')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
		
		</div>
		
		<div class="col-md-6">
		<h3>Daftar Kebutuhan Barang</h3>
		
			<?= $form->field($model, 'jml2')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
		<?= $form->field($model, 'hrg2')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
			<?= $form->field($model, 'ttl2')->widget(yii\widgets\MaskedInput::className(),[
                   'clientOptions' => [
        'alias' =>  'decimal',
        'groupSeparator' => ',',
        'autoGroup' => true
    ],
                ])?>
		</div>
	</div>
		
	<?php 
	$data = ["kd_rek" => "01",
			"kd_rekX" => "02"
	];
	?>
	<?php echo $form->field($model, 'kd_rek')->widget(Select2::classname(), [
    'data' => \app\models\RefRek::getrek(),
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Rekening ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>
	
	<?= $form->field($model, 'ket')->textarea(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
