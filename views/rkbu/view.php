<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rkbu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'RKBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkbu-view">

    <p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_rkbu',
            'kd_skpd',
            'kd_sub2',
            'nm_brg',
            'spek',
            'thn',
            'kd_rek',
        ],
    ]) ?>

</div>
