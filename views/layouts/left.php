
<aside class="main-sidebar">

    <section class="sidebar">

        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="<?= $directoryAsset ?>/img/user2-160x160.jpg" class="img-circle" alt="User Image"/>
            </div>
            <div class="pull-left info">
                <p><?= Yii::$app->user->identity->username ?></p>

                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>

        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search..."/>
                <span class="input-group-btn">
                    <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i class="fa fa-search"></i>
                    </button>
                </span>
            </div>
        </form>
        <!-- /.search form -->

        <?php
        $itemmaster = [
                    ['label' => 'Golongan Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-gol']],
                    ['label' => 'Bidang Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-bidang']],
                    ['label' => 'Kelompok Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-kel']],
                    ['label' => 'Sub Kelompok Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-sub']],
                    ['label' => 'Sub Sub Kelompok Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-subsub']],
                    //['label' => 'Barang Pakai Habis', 'icon' => 'fa fa-circle-o', 'url' => ['/pengadaan']],
                    ['label' => 'Satuan', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-satuan']],
                    //['label' => 'Periode', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
                    //['label' => 'Kode Lokasi SKPD', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
                    ['label' => 'Komponen Pemilik', 'icon' => 'fa fa-circle-o', 'url' => ['/pemilik']],
                    ['label' => 'Rekening', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-rek']],
                    ['label' => 'Asal Usul', 'icon' => 'fa fa-circle-o', 'url' => ['/ref-asal']],
                //['label' => 'Gudang', 'icon' => 'fa fa-file-code-o', 'url' => ['/ref-gudang']],
        ];


        $perencanaan = [
            ['label' => 'RKBU/DKBU/RKBD/DKBD', 'icon' => 'fa fa-circle-o', 'url' => ['/rkbu']],
            ['label' => 'RKPBU/DKPBU/RKPBD/DKPBD', 'icon' => 'fa fa-circle-o', 'url' => ['/rkpbu']],
        ];

        $pengadaan = [
            ['label' => 'Input pengadaan', 'icon' => 'fa fa-circle-o', 'url' => ['/pengadaan']],
            ['label' => 'Daftar Hasil Pengadaan Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/pengadaan/hasilpengadaan']],
            ['label' => 'Buku penerimaan', 'icon' => 'fa fa-circle-o', 'url' => ['/pengadaan/bukuterima']],
        ];

        $penyaluran = [
                    ['label' => 'Input Penyaluran', 'icon' => 'fa fa-circle-o', 'url' => ['/salur']],
                    ['label' => 'Kartu Barang', 'icon' => 'fa fa-circle-o', 'url' => ['/salur/kartu']],
                    ['label' => 'Kartu Persediaan Barang', 'icon' => 'fa fa-circle-o', 'url' => ['salur/persediaan']],
                    ['label' => 'Laporan Stock Opname', 'icon' => 'fa fa-circle-o', 'url' => ['salur/opname']],
                    ['label' => 'Buku Pengeluaran Barang', 'icon' => 'fa fa-circle-o', 'url' => ['salur/bukukeluar']],
                    ['label' => 'Buku Barang Inventaris', 'icon' => 'fa fa-circle-o', 'url' => ['salur/bukuinventaris']],
                    ['label' => 'Buku Barang Habis Pakai', 'icon' => 'fa fa-circle-o', 'url' => ['salur/bukuhabispakai']],
                    ['label' => 'Laporan Semester Inventaris', 'icon' => 'fa fa-circle-o', 'url' => ['salur/laporaninventaris']],
                    ['label' => 'Laporan Semester Habis Pakai', 'icon' => 'fa fa-circle-o', 'url' => ['salur/laporanhabispakai']],
        ];


        $iteminventaris = [
            [
                'label' => 'Input KIB',
                'icon' => 'fa fa-circle-o',
                'url' => '#',
                'items' => [
                    ['label' => 'KIB A', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-a']],
                    ['label' => 'KIB B', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-b']],
                    ['label' => 'KIB C', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-c']],
                    ['label' => 'KIB D', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-d']],
                    ['label' => 'KIB E', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-e']],
                    ['label' => 'KIB F', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-f']],
                ]
            ],
            ['label' => 'Buku Inventaris (BI)', 'icon' => 'fa fa-circle-o', 'url' => ['/bi']],
            ['label' => 'Rekap BI', 'icon' => 'fa fa-circle-o', 'url' => ['/rekap-bi']],
            [
                'label' => 'Kartu Inventaris',
                'icon' => 'fa fa-circle-o',
                'url' => '#',
                'items' => [
                    ['label' => 'KIB A', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-a/lap']],
                    ['label' => 'KIB B', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-b/lap']],
                    ['label' => 'KIB C', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-c/lap']],
                    ['label' => 'KIB D', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-d/lap']],
                    ['label' => 'KIB E', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-e/lap']],
                    ['label' => 'KIB F', 'icon' => 'fa fa-circle-o', 'url' => ['/kib-f/lap']],
                ]
            ],
            ['label' => 'KIR', 'icon' => 'fa fa-circle-o', 'url' => ['/kir']],
            ['label' => 'Status Penggunaan Barang', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Laporan Mutasi Barang', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Laporan Mutasi per SKPD', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Rekap Daftar Mutasi Barang', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Penambahan aset bukan pengadaan', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Daftar aset lain-lainnya', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            //	['label' => 'Barang Dibawah Nilai Kapitalisasi', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            //	['label' => 'Daftar Mutasi Kode Barang', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            //	['label' => 'Daftar Mutasi SKPD Berkurang', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            //	['label' => 'Daftar Mutasi SKPD Bertambah', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            //	['label' => 'Executive Summary', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Barang Yang Digunausahakan', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
            ['label' => 'Barang Milik Daerah Dihapuskan', 'icon' => 'fa fa-circle-o text-red', 'url' => ['#']],
        ];
        if (in_array(Yii::$app->user->id, ['1', '2', '3'])) {
            $itemmenu = ['label' => 'Menu Yii2', 'options' => ['class' => 'header']];
            $itemmenu1 = ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii']];
            $itemmenu2 = ['label' => 'Alur BMD', 'icon' => 'fa fa-dashboard', 'url' => ['/site/alur']];
            $itemmenu3 = ['label' => 'Daftar User', 'icon' => 'fa fa-dashboard', 'url' => ['/user']];
        } else {
            $itemmenu = [];
            $itemmenu1 = [];
            $itemmenu2 = [];
            $itemmenu3 = [];
        }
        ?>

        <?=
        dmstr\widgets\Menu::widget(
                [
                    'options' => ['class' => 'sidebar-menu'],
                    'items' => [
                        [
                            'label' => 'Menu Master',
                            'icon' => 'fa fa-book text-blue',
                            'url' => '#',
                            'items' =>
                            $itemmaster
                        ],
                        [
                            'label' => 'Perencanaan',
                            'icon' => 'fa fa-briefcase text-red',
                            'url' => '#',
                            'items' =>
                            $perencanaan,
                        ],
                        [
                            'label' => 'Pengadaan',
                            'icon' => 'fa fa-star text-green',
                            'url' => '#',
                            'items' => $pengadaan,
                        ],
                        /* [
                          'label' => 'Barang Habis Pakai',
                          'icon' => 'fa fa-dashboard',
                          'url' => '#',
                          'items' => [
                          ['label' => 'Daftar Barang Habis Pakai', 'icon' => 'fa fa-file-code-o', 'url' => ['/pengadaan']],
                          ['label' => 'Buku Penerimaan', 'icon' => 'fa fa-file-code-o', 'url' => ['#']],
                          ],
                          ], */
                        [
                            'label' => 'Penyaluran',
                            'icon' => 'fa fa-paper-plane text-yellow',
                            'url' => '#',
                            'items' => $penyaluran
                        ],
                        [
                            'label' => 'Inventaris',
                            'icon' => 'fa fa-database text-aqua',
                            'url' => '#',
                            'items' => $iteminventaris
                        ],
                        $itemmenu,
                        $itemmenu1,
                        $itemmenu2,
                        $itemmenu3,
                    /* ['label' => 'Login', 'url' => ['site/login'], 'visible' => Yii::$app->user->isGuest],
                      [
                      'label' => 'Same tools',
                      'icon' => 'fa fa-share',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Gii', 'icon' => 'fa fa-file-code-o', 'url' => ['/gii'],],
                      ['label' => 'Debug', 'icon' => 'fa fa-dashboard', 'url' => ['/debug'],],
                      [
                      'label' => 'Level One',
                      'icon' => 'fa fa-circle-o',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Level Two', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      [
                      'label' => 'Level Two',
                      'icon' => 'fa fa-circle-o',
                      'url' => '#',
                      'items' => [
                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      ['label' => 'Level Three', 'icon' => 'fa fa-circle-o', 'url' => '#',],
                      ],
                      ],
                      ],
                      ],
                      ],
                      ], */
                    ],
                ]
        )
        ?>

    </section>

</aside>
