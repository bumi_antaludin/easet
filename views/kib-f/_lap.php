
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) F - Konstruksi dalam Pengerjaan</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						  <tr>
							<th rowspan="2" scope="col">No</th>
							<th rowspan="2" scope="col">Jenis Barang/<br>
							Nama Barang</th>
							<th rowspan="2" scope="col">Bangunan</th>
							<th colspan="2" scope="col">Konstruksi Bangunan</th>
							<th rowspan="2" scope="col">Luas<br>
							(m2)</th>
							<th rowspan="2" scope="col">Letak/Lokasi<br>
							Alamat</th>
							<th colspan="2" scope="col">Dokumen</th>
							<th rowspan="2" scope="col">Tgl,Bln,<br>
							Thn,Mulai</th>
							<th rowspan="2" scope="col">Status<br>
							Tanah</th>
							<th rowspan="2" scope="col">Kode<br>
							Nomor Tanah</th>
							<th rowspan="2" scope="col">Asal Usul<br>
							Pembiayaan</th>
							<th rowspan="2" scope="col">Nilai<br>
							  Kontrak</th>
							<th rowspan="2" scope="col">Ket.</th>
						  </tr>
						  <tr>
							<th>Tingkat/Tidak</th>
							<th>Beton/Tidak</th>
							<th>Tanggal</th>
							<th>Nomor</th>
						  </tr>
						  <tr align="center">
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
							<td>9</td>
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
							<td>15</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td colspan="12">06 - KONSTRUKSI DALAM PENGERJAAN</td>
							<td>3.817.675.406</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td>1</td>
							<td>Gedung Perpustakaan</td>
							<td>P</td>
							<td>Tidak</td>
							<td>Beton</td>
							<td>0</td>
							<td>&nbsp;</td>
							<td>31/12/1980</td>
							<td>&nbsp;</td>
							<td>01/10/2012</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>PENGADAAN</td>
							<td>60.601.535</td>
							<td>&nbsp;</td>
						  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>

