<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibF */

$this->title = 'Ubah KIB F: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB F', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-f-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
