<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibF */

$this->title = 'Tambah KIB F';
$this->params['breadcrumbs'][] = ['label' => 'KIB F', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-f-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
