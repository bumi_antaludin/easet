<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\bootstrap\Carousel;

$this -> title = 'Alur BMD';
?>
<?php 
$imgx =[
'1'=>Html::img('@web/upload/bmd-alur/1.PNG'),   
'2'=>Html::img('@web/upload/bmd-alur/2.PNG'),    
'3'=>Html::img('@web/upload/bmd-alur/3.PNG'),   
'4'=>Html::img('@web/upload/bmd-alur/4.PNG'),    
'5'=>Html::img('@web/upload/bmd-alur/5.PNG'),  
'6'=>Html::img('@web/upload/bmd-alur/6.PNG'),   
'7'=>Html::img('@web/upload/bmd-alur/7.PNG'),    
'8'=>Html::img('@web/upload/bmd-alur/8.PNG'),   
'9'=>Html::img('@web/upload/bmd-alur/9.PNG'),    
'10'=>Html::img('@web/upload/bmd-alur/10.PNG'),  
'12'=>Html::img('@web/upload/bmd-alur/11.PNG'),   
'12'=>Html::img('@web/upload/bmd-alur/12.PNG'),    
'13'=>Html::img('@web/upload/bmd-alur/13.PNG'),   
'14'=>Html::img('@web/upload/bmd-alur/14.PNG'),    
'15'=>Html::img('@web/upload/bmd-alur/15.PNG'),  
'16'=>Html::img('@web/upload/bmd-alur/16.PNG'),   
'17'=>Html::img('@web/upload/bmd-alur/17.PNG'),    
'18'=>Html::img('@web/upload/bmd-alur/18.PNG'),   
'19'=>Html::img('@web/upload/bmd-alur/19.PNG'),    
'20'=>Html::img('@web/upload/bmd-alur/20.PNG'),     
]
?>
<div class="row">
     <div class="col-md-12">
<a href=<?php echo yii\helpers\Url::to(['/upload/file.rar']) ?> class="btn btn-success" > link Download</a>
     </div>
</div>
<br>
<div class="container">
    
    <div class="row clearfix">
        <div class="col-md-12 column">
                <?php echo Carousel::widget(
            ['items' => [
            
    //         $content,
            ['content' => $imgx[1],
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            'options' => [
                  'style'=>'width:900px;height:900px',
                'interval' => '600']
            ],      
              
            ['content' => $imgx[2],
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            'options' => [
                  'style'=>'width:900px;height:900px',
                'interval' => '600']
            ],
            ['content' => $imgx[3],
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            'options' => [
                  'style'=>'width:900px;height:900px',
                'interval' => '600']
            ],
            ['content' =>$imgx[4],
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            'options' => [
                  'style'=>'width:900px;height:900px',
                'interval' => '600']
            ],
            ['content' =>$imgx[5],
            'caption' => '<h4>This is title</h4><p>This is the caption text</p>',
            'options' => [
                  'style'=>'width:900px;height:900px',
                'interval' => '600']
            ],
            ]
            ]); ?>
       </div>
   </div>
</div>