<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RefBidang */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bidang Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bidang-view">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-success']) ?>
        
        <?= Html::a('<i class="fa fa-edit"></i> Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
	<?= Html::a('<i class="fa fa-bars"></i> Daftar Bidang', ['index'], ['class' => 'btn btn-info']) ?>
        	<?= Html::a('<i class="fa fa-plus"></i> Tambah Kelompok', ['ref-kel/create'], ['class' => 'btn btn-warning']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_gol',
            'kd_bid',
            'uraian',
        ],
    ]) ?>

</div>
