<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bidang Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-bidang-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Bidang', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'kd_gol', 'format' => 'text', 'label' => 'KODE GOLONGAN','headerOptions'=>['width'=>'43%']],
            ['attribute' => 'kd_bid', 'format' => 'text', 'label' => 'KODE BIDANG','headerOptions'=>['width'=>'44%']],
			/*
			//        'id',
            [
                'header'=>'Kode Golongan',
                'value'=>'kd_gol'
            ],
			//   'kd_gol',
            [
                'header'=>'Kode Bidang',
              //  'name'=>'kd_bid',
                'value'=>'kd_bid'
                ],
           // 'kd_bid',
            'uraian',
			*/
            ['class' => 'yii\grid\ActionColumn',
			'contentOptions'=>['style'=>'width: 13%;'],
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'view'=>  function ($url,$model){
						return yii\bootstrap\Html::a('<i class="fa fa-bars"></i>',$url,[
							'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'update'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',$url,[
						'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'delete'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',$url,[
					'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
					'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
							]
						]);
                    }
                ]
			],
        ],
    ]); ?>
	</div>
</div>
