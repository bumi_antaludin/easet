<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\RefBidang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-bidang-form">

    <?php $form = ActiveForm::begin(); ?>
	
	
	<?php echo $form->field($model, 'kd_gol')->widget(Select2::classname(), [
    'data' => app\models\RefGol::getGol(),
    'language' => 'en',
    'options' => [
        'placeholder' => 'Pilih Kode Golongan ...',
         'onchange' => '$.post( "'.Yii::$app->request->BaseUrl.'/ref-bidang/get-bid?id='.'"+$(this).val(), function( data ) {
$("input#kd_bid" ).val(data);
});
'
        ],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'kd_bid')->textInput(['readonly' => true,'maxlength' => true,'id'=>'kd_bid']) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
