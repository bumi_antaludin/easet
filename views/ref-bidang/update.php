<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefBidang */

$this->title = 'Ubah Bidang: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ref Bidangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-bidang-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
