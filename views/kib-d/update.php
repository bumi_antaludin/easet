<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibD */

$this->title = 'Ubah KIB D: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB D', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-d-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
