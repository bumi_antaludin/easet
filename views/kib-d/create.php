<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibD */

$this->title = 'Tambah KIB D';
$this->params['breadcrumbs'][] = ['label' => 'KIB D', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-d-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
