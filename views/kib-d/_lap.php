
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) D - Jalan, irigasi dan Jembatan</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						      <tr>
								<th rowspan="2" scope="col">No</th>
								<th rowspan="2" scope="col">Jenis Barang/<br>
								Nama Barang</th>
								<th colspan="2" scope="col">Nomor</th>
								<th rowspan="2" scope="col">Konstruksi</th>
								<th rowspan="2" scope="col">Panjang<br>
								(m)</th>
								<th rowspan="2" scope="col">Lebar<br>
								(m)</th>
								<th rowspan="2" scope="col">Luas<br>
								(m2)</th>
								<th rowspan="2" scope="col">Letak/Lokasi<br>
								Alamat</th>
								<th colspan="2" scope="col">Dokumen</th>
								<th rowspan="2" scope="col">Status<br>
							Tanah</th>
								<th rowspan="2" scope="col">Nomor Kode<br>
							Tanah</th>
								<th rowspan="2" scope="col">Asal Usul</th>
								<th rowspan="2" scope="col">Harga</th>
								<th rowspan="2" scope="col">Kondisi<br>
								(B,KB,RKB)</th>
								<th rowspan="2" scope="col">Ket.</th>
							  </tr>
							  <tr>
								<th>Kode Barang</th>
								<th>Registrasi</th>
								<th>Tanggal</th>
								<th>Nomor</th>
							  </tr>
							  <tr align="center">
								<td>1</td>
								<td>2</td>
								<td>3</td>
								<td>4</td>
								<td>5</td>
								<td>6</td>
								<td>7</td>
								<td>8</td>
								<td>9</td>
								<td>10</td>
								<td>11</td>
								<td>12</td>
								<td>13</td>
								<td>14</td>
								<td>15</td>
								<td>16</td>
								<td>17</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
							  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
