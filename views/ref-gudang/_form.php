<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefGudang */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-gudang-form">

    <?php $form = ActiveForm::begin([
        'enableAjaxValidation'=>true
        
    ]); ?>
    
     <?= $form->errorSummary($model); ?>
	<?= $form->field($model,'kd_brg')->textInput(['maxlength'=>true]) ?>			
    <?= $form->field($model, 'kd_gudang')->textInput() ?>
        
    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
