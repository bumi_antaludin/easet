<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefGudang */

$this->title = 'Create Ref Gudang';
$this->params['breadcrumbs'][] = ['label' => 'Ref Gudangs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-gudang-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
