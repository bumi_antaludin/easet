<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefAsal */

$this->title = 'Tambah Asal';
$this->params['breadcrumbs'][] = ['label' => 'Asal-Usul Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-asal-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
