<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Asal-Usul Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-asal-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Asal', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'kd_asal', 'format' => 'text', 'label' => 'KODE ASAL','headerOptions'=>['width'=>'40%']],
            ['attribute' => 'uraian', 'format' => 'text', 'label' => 'URAIAN','headerOptions'=>['width'=>'40%']],


            //'id',
            //'kd_asal',
            //'uraian',

         //   'id',
          [
              'header'=>'Kode Asal',
              'value'=>'kd_asal'
          ],
            // 'kd_asal',
            'uraian',

            ['class' => 'yii\grid\ActionColumn',
			'contentOptions'=>['style'=>'width: 13%;'],
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'view'=>  function ($url,$model){
						return yii\bootstrap\Html::a('<i class="fa fa-bars"></i>',$url,[
							'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'update'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',$url,[
						'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'delete'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',$url,[
					'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
					'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
							]
						]);
                    }
                ]
			],
        ],
    ]); ?>
	</div>
</div>
