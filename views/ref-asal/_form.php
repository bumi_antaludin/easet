<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\RefAsal */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-asal-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_asal')->textInput(['maxlength' => true,'readonly'=>true]) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
