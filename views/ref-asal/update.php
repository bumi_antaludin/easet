<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefAsal */

$this->title = 'Ubah Asal-Usul: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Asal-Usul Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-asal-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
