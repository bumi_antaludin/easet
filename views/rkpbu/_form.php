<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Rkpbu */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="rkpbu-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'thn')->textInput() ?>

    <?= $form->field($model, 'kd_rkpbu')->textInput() ?>

    <?php 
	$data = ["kd_skpd" => "01",
			"kd_skpdX" => "02"
	];
	?>
	<?php echo $form->field($model, 'kd_skpd')->widget(Select2::classname(), [
    'data' => $data,
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?php 
	$data = ["kd_sub2" => "01",
			"kd_subX" => "02"
	];
	?>
	<?php echo $form->field($model, 'kd_sub2')->widget(Select2::classname(), [
    'data' => $data,
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Sub-Sub ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'nm_brg')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'spek')->textInput(['maxlength' => true]) ?>
	
	<div class="row">
		<div class="col-md-6">
		<h3>Rencana Kebutuhan Barang</h3>
		
		<?= $form->field($model, 'jml')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'hrg')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'satuan')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'ttl')->textInput(['maxlength' => true]) ?>
		</div>
		
		<div class="col-md-6">
		<h3>Daftar Kebutuhan Barang</h3>
		
		<?= $form->field($model, 'jml2')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'hrg2')->textInput(['maxlength' => true]) ?>
		<?= $form->field($model, 'ttl2')->textInput(['maxlength' => true]) ?>
		</div>
	</div>
		
	<?php 
	$data = ["kd_rek" => "01",
			"kd_rekX" => "02"
	];
	?>
	<?php echo $form->field($model, 'kd_rek')->widget(Select2::classname(), [
    'data' => $data,
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Rekening ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>
	
	<?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>
	
	<?= $form->field($model, 'sts_pelihara')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
