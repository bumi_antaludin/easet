<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Rkpbu */

$this->title = 'Tambah RKPBU';
$this->params['breadcrumbs'][] = ['label' => 'RKPBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkpbu-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
