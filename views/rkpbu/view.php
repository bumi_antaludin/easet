<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Rkpbu */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'RKPBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="rkpbu-view">

    <p>
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_skp',
            'kd_brg',
            'kd_lks',
            'jml',
            'hrg',
            'jml_biaya',
            'kd_rek',
            'ket',
        ],
    ]) ?>

</div>
