<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Rkpbu */

$this->title = 'Ubah RKPBU: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'RKPBU', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="rkpbu-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
