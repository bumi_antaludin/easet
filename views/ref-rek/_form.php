<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\RefRek */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-rek-form">

    <?php $form = ActiveForm::begin(); ?>
    
    <?= $form->field($model, 'kd_rek')->textInput() ?>

    <?= $form->field($model, 'kd_detail')->textInput() ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
