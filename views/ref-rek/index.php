<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Rekening';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Rekening', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'kd_rek', 'format' => 'text', 'label' => 'KODE REKENING','headerOptions'=>['width'=>'23%']],
			['attribute' => 'kd_detail', 'format' => 'text', 'label' => 'KODE DETAIL','headerOptions'=>['width'=>'23%']],
            ['attribute' => 'uraian', 'format' => 'text', 'label' => 'URAIAN','headerOptions'=>['width'=>'37%']],
            //'id',
            //'kd_rek',
            //'kd_detail',
           // 'uraian',

            ['class' => 'yii\grid\ActionColumn',
			'contentOptions'=>['style'=>'width: 13%;'],
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'view'=>  function ($url,$model){
						return yii\bootstrap\Html::a('<i class="fa fa-bars"></i>',$url,[
							'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'update'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',$url,[
						'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'delete'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',$url,[
					'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
					'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
							]
						]);
                    }
                ]
			],
        ],
    ]); ?>
	</div>
</div>
