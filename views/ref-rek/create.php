<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefRek */

$this->title = 'Tambah Rekening';
$this->params['breadcrumbs'][] = ['label' => 'Rekening', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-rek-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
