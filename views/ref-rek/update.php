<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefRek */

$this->title = 'Ubah Rekening: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Rekening', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-rek-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
