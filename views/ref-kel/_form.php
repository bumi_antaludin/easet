<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\RefKel */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-kel-form">

    <?php $form = ActiveForm::begin(); ?>

		<?php echo $form->field($model, 'kd_bid')->widget(Select2::classname(), [
    'data' => \app\models\RefBidang::GetBid(),
    'language' => 'en',
    'options' => [
        'placeholder' => 'Pilih Kode Bidang ...',
        'onChange'=>'$.post( "'.Yii::$app->request->BaseUrl.'/ref-kel/get-kel?kd_bid='.'"+$(this).val(), function( data ) {
$("input#kd_kel" ).val(data);
});'
        ],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'kd_kel')->textInput(['readonly' => true,'maxlength' => true,'id'=>'kd_kel']) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
