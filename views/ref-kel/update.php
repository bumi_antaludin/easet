<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefKel */

$this->title = 'Ubah Kelompok Barang: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Kelompok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="ref-kel-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
