<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Pemilik */

$this->title = 'Ubah Pemilik: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Pemilik', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="pemilik-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
