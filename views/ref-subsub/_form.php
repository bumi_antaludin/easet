<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\RefSubsub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-subsub-form">

    <?php $form = ActiveForm::begin(); ?>

 	<?php echo $form->field($model, 'kd_sub')->widget(Select2::classname(), [
    'data' => \app\models\RefSub::Getsub(),
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Sub ...',
        'onChange'=>'$.post( "'.Yii::$app->request->BaseUrl.'/ref-subsub/get-subsub?kdsub='.'"+$(this).val(), function( data ) {
$("input#kd_sub2" ).val(data);
});'
        ],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>
	
    <?= $form->field($model, 'kd_sub2')->textInput(['readonly' => true,'maxlength' => true,'id'=>'kd_sub2']) ?>
	
    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
