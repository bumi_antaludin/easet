<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefSubsub */

$this->title = 'Ubah Sub-Sub: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sub-Sub Kelompok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-subsub-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
