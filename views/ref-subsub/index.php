<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Sub-Sub Kelompok Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-subsub-index">

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Sub-Sub', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
		'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'kd_sub', 'format' => 'text', 'label' => 'KODE SUB','headerOptions'=>['width'=>'25%']],
            ['attribute' => 'kd_sub2', 'format' => 'text', 'label' => 'KODE SUB SUB','headerOptions'=>['width'=>'27%']],
            ['attribute' => 'uraian', 'format' => 'text', 'label' => 'URAIAN','headerOptions'=>['width'=>'25%']],

         //   'id',
           // 'kd_sub',
           // 'kd_sub2',

            ['class' => 'yii\grid\ActionColumn',
			'contentOptions'=>['style'=>'width: 13%;'],
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'view'=>  function ($url,$model){
						return yii\bootstrap\Html::a('<i class="fa fa-bars"></i>',$url,[
							'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'update'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',$url,[
						'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'delete'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',$url,[
					'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
					'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
							]
						]);
                    }
                ]
			],
        ],
    ]); ?>
	</div>
</div>
