<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefSubsub */

$this->title = 'Tambah Sub-Sub';
$this->params['breadcrumbs'][] = ['label' => 'Sub-Sub Kelompok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-subsub-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
