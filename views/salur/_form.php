<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Salur */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="salur-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'kd_ada')->textInput() ?>

 	<?php echo $form->field($model, 'kd_brghbs')->widget(Select2::classname(), [
    'data' => app\models\RefSubsub::GetBrgHabis(),  
    'language' => 'en',
    'options' => ['placeholder' => 'Pilih Kode Barang Habis ...'],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'jml')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'hrg')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'ttl')->textInput(['type'=>'number']) ?>

    <?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
<script>
       $('input.number').keyup(function(event) {
       // skip for arrow keys
       if(event.which >= 37 && event.which <= 40) return;
       // format number
       $(this).val(function(index, value) {
             return value
            .replace(/\D/g, '')
            .replace(/\B(?=(\d{3})+(?!\d))/g, ".")
            ;
       });
});
</script>
