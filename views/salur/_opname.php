
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Laporan Stock Opname</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						  <tr>
							<th rowspan="3" scope="col">No</th>
							<th rowspan="3" scope="col">Tanggal</th>
							<th rowspan="3" scope="col">Barang</th>
							<th colspan="4" rowspan="2" scope="col">Saldo Akhir<br>
							31 Desember 2012</th>
							<th colspan="8" scope="col">Saldo per 1 Januari 2013 s/d 1 Januari 2013</th>
							<th colspan="4" rowspan="2" scope="col">Saldo Akhir<br>
							  1 Januari 2013</th>
							<th rowspan="3" scope="col">Keterangan</th>
						  </tr>
						  <tr>
							<th colspan="4">Bertambah</th>
							<th colspan="4">Berkurang</th>
						  </tr>
						  <tr>
							<th>Jumlah</th>
							<th>Harga<br>
							Satuan</th>
							<th>Jumlah<br>
							Harga</th>
							<th>Satuan</th>
							<th>Jumlah</th>
							<th>Harga<br>
							Satuan</th>
							<th>Jumlah<br>
							Harga</th>
							<th>Satuan</th>
							<th>Jumlah</th>
							<th>Harga<br>
							Satuan</th>
							<th>Jumlah<br>
							Harga</th>
							<th>Satuan</th>
							<th>Jumlah</th>
							<th>Harga<br>
							Satuan</th>
							<th>Harga<br>
							  Jumlah</th>
							<th>Satuan</th>
						  </tr>
						  <tr align="center">
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
							<td>9</td>
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
							<td>15</td>
							<td>16</td>
							<td>17</td>
							<td>18</td>
							<td>19</td>
							<td>20</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						  <tr>
							<td colspan="3"><strong>Total</strong></td>
							<td colspan="4">0</td>
							<td colspan="4">0</td>
							<td colspan="4">0</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
