<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Salur */

$this->title = 'Ubah Penyaluran: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Penyaluran', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="salur-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
