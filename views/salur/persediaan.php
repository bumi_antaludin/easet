<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Salur;
?>
<div class="salur-kartu lap">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-3 ">
			  <label>SKPD</label>
		<?= Select2::widget([
				'name' => 'kd_skpd',
				'data' => app\models\RefSkpd::getskpd(),
				'options' => [
					'placeholder' => 'Pilih SKPD ...',
					'multiple' => false,
					],
                                  'pluginOptions' => [
        'allowClear' => true
    ],
				]
				); ?>
		</div>
		<div class="col-md-2 ">
			<?= $form->field($model, 'jns_brg')->dropDownList(["Pakai Habis" => "Pakai Habis", "Inventaris" => "Inventaris"]) ?>
		</div>
		<div class="col-md-3 ">
			<?= $form->field($model, 'nm_brg')->dropDownList(["Nama 1" => "Nama 1", "Nama 2" => "Nama 2"]) ?>
		</div>
		<div class="col-md-4 ">
			<?php
				echo '<label class="control-label">Pilih Tanggal</label>';
				echo DatePicker::widget([
					'model' => $model,
					'attribute' => 'from_date',
					'attribute2' => 'to_date',
					'options' => ['placeholder' => 'Tanggal Awal'],
					'options2' => ['placeholder' => 'Tanggal Akhir'],
					'type' => DatePicker::TYPE_RANGE,
					'form' => $form,
					'pluginOptions' => [
						'format' => 'yyyy-mm-dd',
						'autoclose' => true,
					]
				]);
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
		<?= Html::a('<i class="glyphicon glyphicon-print "></i> Cetak', ['lappersediaan'], ['class' => 'btn btn-success' ,'target'=>'_blank']) ?>

		</div>
	</div>
	
	<div class="jarak15"></div>
	
	<?= $this->render('_persediaan', [
       // 'model' => $model,
    ]) ?>
	
	<?php ActiveForm::end(); ?>
</div>
