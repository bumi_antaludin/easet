<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Salur */

$this->title = 'Penyaluran';
$this->params['breadcrumbs'][] = ['label' => 'Penyaluran', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="salur-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
