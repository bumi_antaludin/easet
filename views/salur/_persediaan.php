
	<div class="row">
		<div class="col-md-12">
			<div class="box">
				<div class="box-header with-border">
				  <h3 class="box-title">Kartu Persediaan Barang</h3>
				</div>
				<div class="box-body">
					<table class="table table-bordered">
					<tbody>
					<tr>
						<th rowspan="2">Tanggal</th>
						<th rowspan="2">No/Tgl Surat Dasar <br>Penerimaan Pengeluaran</th>
						<th rowspan="2">Uraian </th>
						<th colspan="3">Barang-barang</th>
						<th rowspan="2">Harga Satuan</th>
						<th colspan="3">Jumlah Harga Barang Yang<br> Diterima/Dikeluarkan/Sisa</th>
						<th rowspan="2">Keterangan</th>
					</tr>
					<tr>
						<th>Masuk</th>
						<th>Keluar</th>
						<th>Sisa</th>
						<th>Bertambah</th>
						<th>Berkurang</th>
						<th>Sisa</th>
					</tr>
					<tr align="center">
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>5</td>
						<td>6</td>
						<td>7</td>
						<td>8</td>
						<td>9</td>
						<td>10</td>
						<td>11</td>
					</tr>
					<tr>
						<td>00/00/0000</td>
						<td>&nbsp;</td>
						<td>Saldo Akhir</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>0,00</td>
						<td>0</td>
						<td>0</td>
						<td>0</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>

