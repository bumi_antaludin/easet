<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Salur;
?>
<div class="salur-kartu lap">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-6 ">
			  <label>SKPD</label>
		<?= Select2::widget([
				'name' => 'kd_skpd',
				'data' => app\models\RefSkpd::getskpd(),
				'options' => [
					'placeholder' => 'Pilih SKPD ...',
					'multiple' => false,
					],
                                  'pluginOptions' => [
        'allowClear' => true
    ],
				]
				); ?>
		</div>
		<div class="col-md-3 ">
			<?= $form->field($model, 'thn')->dropDownList(["2015" => "2015", "2016" => "2016"]) ?>
		</div>
		
		<div class="col-md-3 ">
			<?= $form->field($model, 'sem')->dropDownList(["Semester 1" => "Semester 1", "Semester 2" => "Semester 2"]) ?>
		</div>
		
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
		<?= Html::a('<i class="glyphicon glyphicon-print "></i> Cetak', ['lapinventaris'], ['class' => 'btn btn-success' ,'target'=>'_blank']) ?>

		</div>
	</div>
	<div class="jarak15"></div>
	
	<?= $this->render('_laporaninventaris', [
     //   'model' => $model,
    ]) ?>
	
	<?php ActiveForm::end(); ?>
</div>
