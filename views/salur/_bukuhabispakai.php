
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Buku Barang Habis Pakai</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						  <tr>
							<th rowspan="3" scope="col">No</th>
							<th colspan="8" scope="col">PENERIMAAN</th>
							<th colspan="4" scope="col">PENGELUARAN</th>
							<th rowspan="3" scope="col">Keterangan</th>
						  </tr>
						  <tr>
							<th rowspan="2">Tanggal<br>
							Diterima</th>
							<th rowspan="2">Nama/Jenis<br>
							Barang</th>
							<th rowspan="2">Merek/<br>
							  Ukuran</th>
							<th rowspan="2">Tahun<br>
						Pembuatan</th>
							<th rowspan="2">Jumlah/<br>
						Satuan</th>
							<th rowspan="2">Tgl/No.Kontrak/<br>
						SP/SPK</th>
							<th colspan="2">B.A Pemeriksaan</th>
							<th rowspan="2">Tanggal<br>
							Dikeluarkan</th>
							<th rowspan="2">Diserahkan<br>
							Kepada</th>
							<th rowspan="2">Jumlah Satuan/<br>
							Barang</th>
							<th rowspan="2">Tgl/No.Surat<br>
							  Penyerahan</th>
						  </tr>
						  <tr>
							<th>Tanggal</th>
							<th>Nomor</th>
						  </tr>
						  <tr align="center">
							<td>1</td>
							<td>2</td>
							<td>3</td>
							<td>4</td>
							<td>5</td>
							<td>6</td>
							<td>7</td>
							<td>8</td>
							<td>9</td>
							<td>10</td>
							<td>11</td>
							<td>12</td>
							<td>13</td>
							<td>14</td>
						  </tr>
						  <tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
						  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
