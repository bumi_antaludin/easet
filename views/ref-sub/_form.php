<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\RefSub */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="ref-sub-form">

    <?php $form = ActiveForm::begin(); ?>

		<?php echo $form->field($model, 'kd_kel')->widget(Select2::classname(), [
    'data' => app\models\RefKel::Getkel(),
    'language' => 'en',
    'options' => [
        'placeholder' => 'Pilih Kode Kelompok ...',
        'onChange'=>'$.post( "'.Yii::$app->request->BaseUrl.'/ref-sub/get-sub?kdkel='.'"+$(this).val(), function( data ) {
$("input#kd_sub" ).val(data);
});'
        ],
    'pluginOptions' => [
        'allowClear' => true
    ],
	]); ?>

    <?= $form->field($model, 'kd_sub')->textInput(['readonly' => true,'maxlength' => true,'id'=>'kd_sub']) ?>

    <?= $form->field($model, 'uraian')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Ubah' : 'Simpan', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
