<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefSub */

$this->title = 'Ubah Sub: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Sub Kelompok Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-sub-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
