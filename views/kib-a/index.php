<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\KibASearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'KIB A';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-a-index">

    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah KIB A', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	<div class="box">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        //'filterModel' => $searchModel,
		'layout' => "{items}\n{pager}",
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
			['attribute' => 'kd_brg', 'format' => 'text', 'label' => 'KODE BARANG','headerOptions'=>['width'=>'10%']],
            ['attribute' => 'kd_lks', 'format' => 'text', 'label' => 'KODE LOKASI','headerOptions'=>['width'=>'15%']],
            ['attribute' => 'kd_skpd', 'format' => 'text', 'label' => 'KODE SKPD','headerOptions'=>['width'=>'15%']],
            ['attribute' => 'tgl_catat', 'format' => 'text', 'label' => 'TANGGAL CATAT','headerOptions'=>['width'=>'10%']],
            ['attribute' => 'ket', 'format' => 'text', 'label' => 'KETERANGAN','headerOptions'=>['width'=>'20%']],
            ['attribute' => 'kondisi', 'format' => 'text', 'label' => 'KONDISI','headerOptions'=>['width'=>'10%']],

            //'id',
            //'kd_brg',
            //'kd_lks',
            //'kd_skpd',
           // 'tgl_catat',
            // 'tgl_oleh',
            // 'nillai_kap',
            // 'nilai_kon',
            // 'nilai_aset',
            // 'nilai_retensi',
            // 'no_reg',
            // 'kd_rek',
            // 'jml',
            // 'no_spk',
            // 'asal',
            // 'status',
            // 'no_ser',
            // 'tgl_ser',
            // 'luas',
            // 'letak',
            // 'guna',
            //'ket',
           // 'kondisi',

            ['class' => 'yii\grid\ActionColumn',
			'contentOptions'=>['style'=>'width: 13%;'],
                'template'=>'{view}{update}{delete}',
                'buttons'=>[
                    'view'=>  function ($url,$model){
						return yii\bootstrap\Html::a('<i class="fa fa-bars"></i>',$url,[
							'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'update'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-edit"></i>',$url,[
						'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
						]);
                    },
                    'delete'=>  function ($url,$model){
					return yii\bootstrap\Html::a('<i class="fa fa-trash"></i>',$url,[
					'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
					'data'=>[
                            'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
                            'method'=>'post',
							]
						]);
                    }
                ]
			],
        ],
    ]); ?>
	</div>
</div>
