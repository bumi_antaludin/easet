<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KibASearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kib-a-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kd_brg') ?>

    <?= $form->field($model, 'kd_lks') ?>

    <?= $form->field($model, 'kd_skpd') ?>

    <?= $form->field($model, 'tgl_catat') ?>

    <?php // echo $form->field($model, 'tgl_oleh') ?>

    <?php // echo $form->field($model, 'nillai_kap') ?>

    <?php // echo $form->field($model, 'nilai_kon') ?>

    <?php // echo $form->field($model, 'nilai_aset') ?>

    <?php // echo $form->field($model, 'nilai_retensi') ?>

    <?php // echo $form->field($model, 'no_reg') ?>

    <?php // echo $form->field($model, 'kd_rek') ?>

    <?php // echo $form->field($model, 'jml') ?>

    <?php // echo $form->field($model, 'no_spk') ?>

    <?php // echo $form->field($model, 'asal') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'no_ser') ?>

    <?php // echo $form->field($model, 'tgl_ser') ?>

    <?php // echo $form->field($model, 'luas') ?>

    <?php // echo $form->field($model, 'letak') ?>

    <?php // echo $form->field($model, 'guna') ?>

    <?php // echo $form->field($model, 'ket') ?>

    <?php // echo $form->field($model, 'kondisi') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
