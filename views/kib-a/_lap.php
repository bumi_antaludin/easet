
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) A - Tanah</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
						<tr>
						<th rowspan="3">No</th>
						<th rowspan="3">Nama barang/<br>
						Jenis barang</th>
						<th colspan="2">Nomor</th>
						<th rowspan="3">Luas (m2)</th>
						<th rowspan="3">Tahun<br>
						Pengadaan</th>
						<th rowspan="3">Letak/<br>
						Alamat</th>
						<th colspan="3">Satuan Tanah</th>
						<th rowspan="3">Penggunaan</th>
						<th rowspan="3">Asal usul</th>
						<th rowspan="3">Harga</th>
						<th rowspan="3">Keterangan</th>
						</tr>
						<tr>
						<th rowspan="2">Kode Barang</th>
						<th rowspan="2">Registrasi</th>
						<th rowspan="2">Hak</th>
						<th colspan="2">Sertifikat</th>
						</tr>
						<tr>
						<th>Tanggal</th>
						<th>Nomor</th>
						</tr>
						<tr align="center">
						<td>1</td>
						<td>2</td>
						<td>3</td>
						<td>4</td>
						<td>5</td>
						<td>6</td>
						<td>7</td>
						<td>8</td>
						<td>9</td>
						<td>10</td>
						<td>11</td>
						<td>12</td>
						<td>13</td>
						<td>14</td>
						</tr>
						<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						</tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>