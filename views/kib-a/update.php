<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibA */

$this->title = 'Ubah KIB A: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB A', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-a-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
