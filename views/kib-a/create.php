<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibA */

$this->title = 'Tambah KIB A';
$this->params['breadcrumbs'][] = ['label' => 'KIB A', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-a-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
