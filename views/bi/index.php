<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Pemilik;
$this->title = 'Buku Inventaris (BI)';
?>
<div class="salur-kartu lap">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-4 ">
			<?= $form->field($model, 'thn')->dropDownList(["2015" => "2015", "2016" => "2016"]) ?>
		</div>
		<div class="col-md-4 ">
			<?= $form->field($model, 'kd_milik')->dropDownList(Pemilik::getPemilik()) ?>
		</div>
		<div class="col-md-4 ">
		<?php echo $form->field($model, 'kd_skpd')->widget(Select2::classname(), [
				'data' => \app\models\RefSkpd::getskpd(),
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
                </div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
                <?= Html::a('<i class="glyphicon glyphicon-print "></i> Cetak',['lapbi'],['class'=>'btn btn-success','target'=>'_blank']) ?>
                </div>
	</div>
	<div class="jarak15"></div>
	
	<?= $this->render('_table', [
     //   'model' => $model,
    ]) ?>
	
	<?php ActiveForm::end(); ?>
</div>
