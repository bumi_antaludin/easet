<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\KibE */

$this->title = 'Tambah KIB E';
$this->params['breadcrumbs'][] = ['label' => 'KIB E', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-e-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
