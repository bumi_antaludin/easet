<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\KibESearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kib-e-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'kd_brg') ?>

    <?= $form->field($model, 'kd_lks') ?>

    <?= $form->field($model, 'kd_skpd') ?>

    <?= $form->field($model, 'tgl_catat') ?>

    <?php // echo $form->field($model, 'tgl_oleh') ?>

    <?php // echo $form->field($model, 'nillai_kap') ?>

    <?php // echo $form->field($model, 'nilai_kon') ?>

    <?php // echo $form->field($model, 'nilai_aset') ?>

    <?php // echo $form->field($model, 'nilai_retensi') ?>

    <?php // echo $form->field($model, 'no_reg') ?>

    <?php // echo $form->field($model, 'kd_rek') ?>

    <?php // echo $form->field($model, 'jml') ?>

    <?php // echo $form->field($model, 'no_spk') ?>

    <?php // echo $form->field($model, 'asal') ?>

    <?php // echo $form->field($model, 'buku_jdl') ?>

    <?php // echo $form->field($model, 'buku_spek') ?>

    <?php // echo $form->field($model, 'brg_asal') ?>

    <?php // echo $form->field($model, 'brg_cipta') ?>

    <?php // echo $form->field($model, 'brg_bhn') ?>

    <?php // echo $form->field($model, 'hw_jns') ?>

    <?php // echo $form->field($model, 'hw_ukuran') ?>

    <?php // echo $form->field($model, 'merk') ?>

    <?php // echo $form->field($model, 'tipe') ?>

    <?php // echo $form->field($model, 'no_seri') ?>

    <?php // echo $form->field($model, 'ukuran') ?>

    <?php // echo $form->field($model, 'bahan') ?>

    <?php // echo $form->field($model, 'kondisi') ?>

    <?php // echo $form->field($model, 'ket') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-default']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
