<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\KibE */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="kib-e-form">

    <?php $form = ActiveForm::begin(); ?>
	
	<div class="row">
		<div class="col-md-6">

		
				<?php echo $form->field($model, 'kd_brg')->widget(Select2::classname(), [
				'data' => \app\models\RefSubsub::Getsubsub(),
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode Barang ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
			
			<?php 
				$data = ["kd_lks" => "01",
						"kd_lksX" => "02"
				];
				?>
				<?php echo $form->field($model, 'kd_lks')->widget(Select2::classname(), [
				'data' => $data,
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode Pemilik ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
	
				<?php echo $form->field($model, 'kd_skpd')->widget(Select2::classname(), [
				'data' => \app\models\RefSkpd::getskpd(),
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
			
			<?= $form->field($model, 'tgl_catat')->widget(DatePicker::classname(), [
				'name' => 'tgl_catat', 
				'type' => DatePicker::TYPE_COMPONENT_APPEND,
				'value' => '01-Jan-2016',
				'options' => ['placeholder' => 'Pilih Tanggal'],
				'pluginOptions' => [
					'format' => 'dd-M-yyyy',
					'todayHighlight' => true,
					'autoclose'=>true
				]
				]); 
			?>
			
			<?= $form->field($model, 'tgl_oleh')->widget(DatePicker::classname(), [
				'name' => 'tgl_oleh', 
				'type' => DatePicker::TYPE_COMPONENT_APPEND,
				'value' => '01-Jan-2016',
				'options' => ['placeholder' => 'Pilih Tanggal'],
				'pluginOptions' => [
					'format' => 'dd-M-yyyy',
					'todayHighlight' => true,
					'autoclose'=>true
				]
				]); 
			?>

			<?= $form->field($model, 'nillai_kap')->textInput() ?>

			<?= $form->field($model, 'nilai_kon')->textInput() ?>

			<?= $form->field($model, 'nilai_aset')->textInput() ?>

			<?= $form->field($model, 'nilai_retensi')->textInput() ?>

			<?= $form->field($model, 'no_reg')->textInput(['maxlength' => true]) ?>

			<?php 
				$data = ["kd_rek" => "01",
						"kd_rekX" => "02"
				];
				?>
				<?php echo $form->field($model, 'kd_rek')->widget(Select2::classname(), [
				'data' => $data,
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode Rek ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>

			<?= $form->field($model, 'jml')->textInput() ?>

			<?= $form->field($model, 'no_spk')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'asal')->textInput(['maxlength' => true]) ?>
		</div>
		<div class="col-md-6">
		
			<?= $form->field($model, 'buku_jdl')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'buku_spek')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'brg_asal')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'brg_cipta')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'brg_bhn')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'hw_jns')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'hw_ukuran')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'merk')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'tipe')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'no_seri')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'ukuran')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'bahan')->textInput(['maxlength' => true]) ?>

			<?= $form->field($model, 'kondisi')->dropDownList(["0" => "Baik", "1" => "Rusak Ringan", "2" => "Rusak Berat", "3" => "Tidak Diketahui"]) ?>

			<?= $form->field($model, 'ket')->textInput(['maxlength' => true]) ?>
		</div>
	</div>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Simpan' : 'Ubah', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
