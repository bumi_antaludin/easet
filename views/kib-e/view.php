<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\KibE */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB E', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="kib-e-view">

    <p>
        
        <?= Html::a('<i class="fa fa-plus"></i> Tambah', ['create'], ['class' => 'btn btn-success']) ?>
		<?= Html::a('<i class="fa fa-edit"></i> Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('<i class="fa fa-trash"></i> Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('<i class="fa fa-bars"></i> Daftar KIB-E', ['index'], ['class' => 'btn btn-info']) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_brg',
            'kd_lks',
            'kd_skpd',
            'tgl_catat',
            'tgl_oleh',
            'nillai_kap',
            'nilai_kon',
            'nilai_aset',
            'nilai_retensi',
            'no_reg',
            'kd_rek',
            'jml',
            'no_spk',
            'asal',
            'buku_jdl',
            'buku_spek',
            'brg_asal',
            'brg_cipta',
            'brg_bhn',
            'hw_jns',
            'hw_ukuran',
            'merk',
            'tipe',
            'no_seri',
            'ukuran',
            'bahan',
            'kondisi',
            'ket',
        ],
    ]) ?>

</div>
