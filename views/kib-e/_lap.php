
	<div class="row">
		<div class="col-md-12">
			<div class="box">
					<div class="box-header with-border">
					  <h3 class="box-title">Kartu Inventaris Barang (KIB) E - Asset Tetap Lainnya</h3>
					</div>
					<div class="box-body kelebihan">
						<table class="table table-bordered">
						<tbody>
							  <tr>
								<th rowspan="2" scope="col">No</th>
								<th rowspan="2" scope="col">Jenis Barang/<br>
								Nama Barang</th>
								<th colspan="2" scope="col">Nomor</th>
								<th colspan="2" scope="col">Buku/Perpustakaan</th>
								<th colspan="3" scope="col">Barang Bercorak<br>
								Kesenian/Kebudayaan</th>
								<th colspan="2" scope="col">Hewan/Ternak<br>
								dan Tumbuhan</th>
								<th rowspan="2" scope="col">Jumlah</th>
								<th rowspan="2" scope="col">Tahun Cetak/<br>
								Pembelian</th>
								<th rowspan="2" scope="col">Asal Usul<br>
								  Cara Perolehan</th>
								<th rowspan="2" scope="col">Harga</th>
								<th rowspan="2" scope="col">Ket.</th>
							  </tr>
							  <tr>
								<th>Kode Barang</th>
								<th>Registrasi</th>
								<th>Judul/Pencipta</th>
								<th>Spesisikasi</th>
								<th>Asal Daerah</th>
								<th>Pencipta</th>
								<th>Bahan</th>
								<th>Jenis</th>
								<th>Ukuran</th>
							  </tr>
							  <tr align="center">
								<td>1</td>
								<td>2</td>
								<td>3</td>
								<td>4</td>
								<td>5</td>
								<td>6</td>
								<td>7</td>
								<td>8</td>
								<td>9</td>
								<td>10</td>
								<td>11</td>
								<td>12</td>
								<td>13</td>
								<td>14</td>
								<td>15</td>
								<td>16</td>
							  </tr>
							  <tr>
								<td>&nbsp;</td>
								<td colspan="13">05.17.01.01.01 - Ilmu Pengetahuan Umum</td>
								<td>Rp. 5.200,00</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>1</td>
								<td>Ilmu Pengetahuan Umum</td>
								<td>05.17.01.01.01</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>242</td>
								<td>2008</td>
								<td>&nbsp;</td>
								<td>50.000.000</td>
								<td>&nbsp;</td>
							  </tr>
							  <tr>
								<td>2</td>
								<td>Ilmu Pengetahuan Umum</td>
								<td>05.17.01.01.01</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>&nbsp;</td>
								<td>4243</td>
								<td>2008</td>
								<td>&nbsp;</td>
								<td>50.000.000</td>
								<td>&nbsp;</td>
							  </tr>
						</tbody>
						</table>
					</div>
			</div>
		</div>
	</div>
