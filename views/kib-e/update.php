<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\KibE */

$this->title = 'Ubah KIB E: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'KIB E', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="kib-e-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
