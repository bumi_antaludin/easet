<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use app\models\Kir;
?>
<div class="salur-kartu lap">
	<?php $form = ActiveForm::begin(); ?>
	<div class="row">
		<div class="col-md-12 ">
		<?php echo $form->field($model, 'kd_skpd')->label('Kode SKPD')->widget(Select2::classname(), [
				'data' => \app\models\RefSkpd::getskpd(),
				'language' => 'en',
				'options' => ['placeholder' => 'Pilih Kode SKPD ...'],
				'pluginOptions' => [
					'allowClear' => true
				],
				]); 
			?>
		</div>
	</div>
	<div class="row">
		<div class="col-md-12">
		<button class="btn btn-primary"><span class="glyphicon glyphicon-search"></span> Tampilkan</button>
		<?= Html::a('<i class="glyphicon glyphicon-print"> Cetak</i>', ['lapkir'], ['class'=>'btn btn-success','target'=>'_blank'])?>
                
                </div>
	</div>
	<div class="jarak15"></div>
	
	<?= $this->render('_table', [
     //   'model' => $model,
    ]) ?>
	
	<?php ActiveForm::end(); ?>
</div>
