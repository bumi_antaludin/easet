<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\RefGol */

$this->title = 'Tambah Golongan Barang';
$this->params['breadcrumbs'][] = ['label' => 'Ref Gols', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-gol-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
