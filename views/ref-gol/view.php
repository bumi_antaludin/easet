<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\RefGol */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Golongan Barang', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-gol-view">

    <p>
        <?= Html::a('Tambah', ['create'], ['class' => 'btn btn-warning']) ?>
        
        <?= Html::a('Ubah', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Hapus', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
<?= Html::a('Daftar Golongan', ['index'], ['class' => 'btn btn-info']) ?>
        
		<?= Html::a('Tambah Bidang Barang', ['ref-bidang/create'], ['class' => 'btn btn-success']) ?>
    
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'kd_gol',
            'uraian',
        ],
    ]) ?>

</div>
