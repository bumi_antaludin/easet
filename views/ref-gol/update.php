<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\RefGol */

$this->title = 'Ubah Golongan Barang: ' . ' ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Ref Gols', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Ubah';
?>
<div class="ref-gol-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
