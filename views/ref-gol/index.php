<?php

use yii\helpers\Html;
//use yii\grid\GridView;
use kartik\grid\GridView;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Golongan Barang';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="ref-gol-index">
    <p>
        <?= Html::a('<i class="fa fa-plus"></i> Tambah Golongan', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
	
		<?= GridView::widget([
			'dataProvider' => $dataProvider,
			            'layout'=>"{summary}\n{items}\n{pager}",
                        'pjax'=>true,
            'export'=>false,
			'columns' => [
				['class' => 'yii\grid\SerialColumn'],
				['attribute' => 'kd_gol', 'format' => 'text', 'label' => 'KODE GOLONGAN','headerOptions'=>['width'=>'15%']],
				['attribute' => 'uraian', 'format' => 'text', 'label' => 'URAIAN','headerOptions'=>['width'=>'72%']],

				['class' => 'yii\grid\ActionColumn',
					'contentOptions'=>['style'=>'width: 13%;'],
					'template'=>'{view}{update}{delete}',
					'buttons'=>[
						'view'=>  function ($url,$model){
							return Html::a('<i class="fa fa-bars"></i>',$url,[
								'title'=>  Yii::t('yii', 'Tampil'), 'class'=>'btn btn-success btn-flat', 'style'=>'width: 33%',
							]);
						},
						'update'=>  function ($url,$model){
						return Html::a('<i class="fa fa-edit"></i>',$url,[
							'title'=>  Yii::t('yii', 'Ubah'), 'class'=>'btn btn-primary btn-flat', 'style'=>'width: 33%',
							]);
						},
						'delete'=>  function ($url,$model){
						return Html::a('<i class="fa fa-trash"></i>',$url,[
						'title'=>  Yii::t('yii', 'Hapus'), 'class'=>'btn btn-danger btn-flat', 'style'=>'width: 33%',
						'data'=>[
								'confirm'=>'Apakah Anda Yakin Ingin Hapus Item ini ?',
								'method'=>'post',
								]
							]);
						}
					]
				],
			],
		]); ?>

</div>
