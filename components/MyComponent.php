<?php
namespace app\components;

use Yii;
use yii\base\Component;
use yii\helpers\Html;
class MyComponent extends Component{ 
    
    function tambah_nol($angka,$jumlah)
{
   $jumlah_nol = strlen($angka);
   $angka_nol = $jumlah - $jumlah_nol;
   $nol = "";
   for($i=1;$i<=$angka_nol;$i++)
   {
      $nol .= '0';
   }
   return $nol.$angka;
}

function kode_lokasi($kdpemilik,$kdprov,$kdkab,$kdbid,$kdskpd,$thnoleh,$kdsubunit){
    $kdlokasi = $kdpemilik .'.'. $kdprov .'.'. $kdkab .'.'. $kdbid .'.'. $kdskpd .'.'. $thnoleh .'.'. $kdsubunit;
    return $kdlokasi;
}

}

