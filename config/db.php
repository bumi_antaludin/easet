<?php

return [
    'class' => 'yii\db\Connection',
    'dsn' => 'mysql:host=localhost;dbname=simaset',
    'username' => 'root',
    'password' => 'toor',
    'charset' => 'utf8',
    'enableSchemaCache'=>true,
    'schemaCacheDuration'=>3600,
    'schemaCache'=>'cache'
];
