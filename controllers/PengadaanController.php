<?php

namespace app\controllers;

use Yii;
use mPDF;
use app\models\Pengadaan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * PengadaanController implements the CRUD actions for Pengadaan model.
 */
class PengadaanController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Pengadaan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Pengadaan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionBukuterima()
    {
		$model = New Pengadaan(); 
        return $this->render('bukuterima', [
		'model' => $model,
		]);
    }
	public function actionHasilpengadaan()
    {
		$model = New Pengadaan(); 
        return $this->render('hasilpengadaan', [
		'model' => $model,
		]);
    }
    /**
     *  Laporan hasil Pengadaan
     */
    public function actionTopdf(){
        
        $mpdf = New mPDF('c','A4-L');
        $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_hasilpengadaan',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
/**
 * Laporan Buku terima
 */
     public function actionTopdf2(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_bukuterimapdf',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    
    /**
     * Displays a single Pengadaan model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Pengadaan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Pengadaan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Pengadaan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Pengadaan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Pengadaan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Pengadaan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Pengadaan::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
