<?php

namespace app\controllers;

use Yii;
use app\models\RefSub;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefSubController implements the CRUD actions for RefSub model.
 */
class RefSubController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefSub models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefSub::find()->orderBy(['kd_kel'=>SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefSub model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefSub model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefSub();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $kdkel = $model->kd_kel;
            $kdsub = $model->kd_sub;
            $model->kd_kel = $kdkel;
            $model->kd_sub = $kdkel .'.'. $kdsub;
            $model->save(false);
            
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefSub model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefSub model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetSub($kdkel)
    {
        $cSub = RefSub::find()
                ->select(['kd_kel','kd_sub','uraian'])
                ->where(['kd_kel'=>$kdkel])
                ->count();
        
        if ($cSub >0){
            $sub = RefSub::find()
                    ->select(['kd_kel','kd_sub','uraian'])
                    ->where(['kd_kel'=>$kdkel])
                    ->all();
            
            foreach ($sub as $key => $value) {
                $count = substr($value['kd_sub'],-2);
            }
          
        }else{
            $count ="0";
        }
        return \Yii::$app->MyComponent->tambah_nol($count+1,2);
    }

    /**
     * Finds the RefSub model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefSub the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefSub::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
