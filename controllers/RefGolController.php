<?php

namespace app\controllers;

use Yii;
use app\models\RefGol;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefGolController implements the CRUD actions for RefGol model.
 */
class RefGolController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefGol models.
     * @return mixed
     */
    public function actionIndex()
    {
        $query =  RefGol::find();
        $countQuery = clone $query;
        
        $pages = new \yii\data\Pagination(['totalCount'=>$countQuery->count()]);
        $dataProvider = new ActiveDataProvider([
            'query' =>$query,
           'pagination' => [
        'pageSize' => 10,
    ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'pages'=>$pages,
        ]);
    }

    /**
     * Displays a single RefGol model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefGol model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefGol();
        $countx = RefGol::find()
                ->select(['kd_gol'])
                ->orderBy(['id'=>SORT_DESC])
                ->limit(1)
                ->all();
       
        if ($model->load(Yii::$app->request->post())) {
            $model->save();
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            if (count($countx)>0){
            foreach($countx as $data=>$value){
                $count=$value['kd_gol'];
            }
            }else{
                $count='0';
            }
            $model->kd_gol = \Yii::$app->MyComponent->tambah_nol($count+1,2);
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefGol model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefGol model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefGol model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefGol the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefGol::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
