<?php

namespace app\controllers;

use Yii;
use app\models\RefSubsub;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefSubsubController implements the CRUD actions for RefSubsub model.
 */
class RefSubsubController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefSubsub models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefSubsub::find()
                ->select(['kd_sub','kd_sub2','uraian'])
                ->orderBy(['kd_sub'=>SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefSubsub model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefSubsub model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefSubsub();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $kdsub = $model->kd_sub;
            $kdsubsub = $model->kd_sub2;
            $model->kd_sub = $kdsub;
            $model->kd_sub2 = $kdsub .'.'. $kdsubsub;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing RefSubsub model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefSubsub model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionGetSubsub($kdsub){
        
        $sub = RefSubsub::find()
                ->select(['kd_sub','kd_sub2','uraian'])
                ->where(['kd_sub'=>$kdsub])
                ->count();
        if ($sub>0)
        {
            $subsub = RefSubsub::find()
                    ->select(['kd_sub','kd_sub2'])
                    ->where(['kd_sub'=>$kdsub])
            ->all();
            
            foreach ($subsub as $key => $value) {
                $count = substr($value['kd_sub2'],-2);
            }
        }else{
            $count="0";
        }
        return \Yii::$app->MyComponent->tambah_nol($count+1,2);
                
    }
    /**
     * Finds the RefSubsub model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefSubsub the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefSubsub::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
