<?php

namespace app\controllers;

use Yii;
use mPDF;
use app\models\Salur;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * SalurController implements the CRUD actions for Salur model.
 */
class SalurController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all Salur models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Salur::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }
	
	public function actionKartu()
    {
		$model = New Salur(); 
        return $this->render('kartu', [
		'model' => $model,
		]);
    }
	
	public function actionPersediaan()
    {
		$model = New Salur(); 
        return $this->render('persediaan', [
		'model' => $model,
		]);
    }
	
	public function actionOpname()
    {
		$model = New Salur(); 
        return $this->render('opname', [
		'model' => $model,
		]);
    }
	
	public function actionBukukeluar()
    {
		$model = New Salur(); 
        return $this->render('bukukeluar', [
		'model' => $model,
		]);
    }
	
	public function actionBukuinventaris()
    {
		$model = New Salur(); 
        return $this->render('bukuinventaris', [
		'model' => $model,
		]);
    }
	
	public function actionBukuhabispakai()
    {
		$model = New Salur(); 
        return $this->render('bukuhabispakai', [
		'model' => $model,
		]);
    }
	
	public function actionLaporaninventaris()
    {
		$model = New Salur(); 
        return $this->render('laporaninventaris', [
		'model' => $model,
		]);
    }
	
	public function actionLaporanhabispakai()
    {
		$model = New Salur(); 
        return $this->render('laporanhabispakai', [
		'model' => $model,
		]);
    }
/*
 * Lap Buku Inventaris
 */
      public function actionLapbukuinventaris(){
        
        $mpdf = New mPDF('c','A4-L');
              $mpdf->writeHTML(
        $this->renderPartial('_bukuinventaris',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    
    /*
 * Lap Buku Habis Pakai
 */
      public function actionLapbukuhabispakai(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_bukuhabispakai',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    
   /*
 * Lap Buku Keluar
 */
      public function actionLapbukukeluar(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_bukukeluar',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    } 
   
    /*
 * Lap Kartu
 */
      public function actionLapkartu(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_kartu',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    
    /*
 * Lap Barang Habis Pakai
 */
      public function actionLaphabispakai(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_laporanhabispakai',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    /*
 * Lap inventaris
 */
      public function actionLapinventaris(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_laporaninventaris',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    /*
 * Lap opname
 */
      public function actionLapopname(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_opname',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    /*
 * Lap Persediaan
 */
      public function actionLappersediaan(){
        
        $mpdf = New mPDF('c','A4-L');
          $mpdf->WriteHTML(file_get_contents('../vendor/almasaeed2010/adminlte/bootstrap/css/bootstrap.css'), 1);
              $mpdf->writeHTML(
        $this->renderPartial('_persediaan',[
            'data'=>''
        ],TRUE)
                );
        
        $mpdf->Output();
        exit();
    }
    /**
     * Displays a single Salur model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Salur model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Salur();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Salur model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Salur model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Salur model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Salur the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Salur::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
