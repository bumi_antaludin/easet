<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\helpers\Html;
class SiteController extends Controller
{
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout','index','login'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                       [
                        'actions' => ['index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                    [
                        'actions'=>['login'],
                        'allow'=>true,
                        'roles'=>['?'],
                    ]
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {
        return $this->render('index');
    }

    public function actionAlur()
    {
    /*    $array =[
'1'=>Html::img('@web/upload/bmd-alur/1.PNG'),   
'2'=>Html::img('@web/upload/bmd-alur/2.PNG'),    
'3'=>Html::img('@web/upload/bmd-alur/3.PNG'),   
'4'=>Html::img('@web/upload/bmd-alur/4.PNG'),    
'5'=>Html::img('@web/upload/bmd-alur/5.PNG'),  
'6'=>Html::img('@web/upload/bmd-alur/6.PNG'),   
'7'=>Html::img('@web/upload/bmd-alur/7.PNG'),    
'8'=>Html::img('@web/upload/bmd-alur/8.PNG'),   
'9'=>Html::img('@web/upload/bmd-alur/9.PNG'),    
'10'=>Html::img('@web/upload/bmd-alur/10.PNG'),  
'11'=>Html::img('@web/upload/bmd-alur/11.PNG'),   
'12'=>Html::img('@web/upload/bmd-alur/12.PNG'),    
'13'=>Html::img('@web/upload/bmd-alur/13.PNG'),   
'14'=>Html::img('@web/upload/bmd-alur/14.PNG'),    
'15'=>Html::img('@web/upload/bmd-alur/15.PNG'),  
'16'=>Html::img('@web/upload/bmd-alur/16.PNG'),   
'17'=>Html::img('@web/upload/bmd-alur/17.PNG'),    
'18'=>Html::img('@web/upload/bmd-alur/18.PNG'),   
'19'=>Html::img('@web/upload/bmd-alur/19.PNG'),    
'20'=>Html::img('@web/upload/bmd-alur/20.PNG'),     
];
     * 
     */
       $array = [
    ['id' => 'content', 'name' => Html::img('@web/upload/bmd-alur/1.PNG'), 'class' => 'x'],
    ['id' => 'caption', 'name' => 'ada', 'class' => 'x'],
    ['id' => 'option', 'name' => ['style'=>'width:90px'], 'class' => 'y'],
            
     
            ];

$result = \yii\helpers\ArrayHelper::map($array, 'id', 'name');

        return $this->render(
                'alur',
                [
                    'content'=>$result
                ]
                );
    }

    public function actionLogin()
    {
        if (!\Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    public function actionSignup()
    {
      //  $this->layout="main_signup";
        $model = New \app\models\SignupForm();
        if ($model->load(Yii::$app->request->post()))
        {
            if ($user = $model->signup())
            {
                if (Yii::$app->getUser()->login($user))
                {
                    return $this->goHome();
                }
            }
        }
        return $this->render('signup',[
            'model'=>$model,
        ]);
    }
    

    public function actionAbout()
    {
        return $this->render('about');
    }
}
