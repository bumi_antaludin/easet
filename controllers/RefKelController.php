<?php

namespace app\controllers;

use Yii;
use app\models\RefKel;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefKelController implements the CRUD actions for RefKel model.
 */
class RefKelController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefKel models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefKel::find()->orderBy(['kd_bid'=>SORT_ASC]),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefKel model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefKel model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefKel();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            
            $kdkel = $model->kd_kel;
            $kdbid  = $model->kd_bid;
            $model->kd_bid = $kdbid;
            $model->kd_kel = $kdbid .'.'. $kdkel;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

public function actionGetKel($kd_bid){
    $kel = RefKel::find()
            ->where(['kd_bid'=>$kd_bid])
            ->count();
    if ($kel>0){
        $kdkel = RefKel::find()
                ->select(['kd_kel'])
                ->where(['kd_bid'=>$kd_bid])
                ->orderBy(['kd_kel'=>SORT_DESC])
                ->limit(1)
                ->all();
        
       foreach ($kdkel as $data=>$value) {
       $count=  substr($value["kd_kel"],-2);
       }
    }else{
        $count = "0";
    }
    return \Yii::$app->MyComponent->tambah_nol($count+1,2);
}

/**
     * Updates an existing RefKel model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post())) {
    
            $kdkel = $model->kd_kel;
            $kdbid = $model->kd_bid;
            $model->kd_bid  = $kdbid;
            $model->kd_kel  = $kdbid .'.'. $kdkel;
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefKel model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefKel model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefKel the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefKel::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
