<?php

namespace app\controllers;

use Yii;
use app\models\Rkbu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\SqlDataProvider;
/**
 * RkbuController implements the CRUD actions for Rkbu model.
 */
class RkbuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     *  Query List RKBU
     */
    
    public function listrkbu($kdlks="' '",$thn="' '",$jnsdata=1){
        switch ($jnsdata) {
            case 1:  //seluruhan
                $filter =" ";

                break;
            case 2: // Barang Inventaris
                $filter =" AND kdbrg = ";
                break;
            case 3: //Barang Habis Pakai
                    $filter = " AND left(Kd_sub2,1) = '5' ";
                break;
        }
                $cnt = "select count(*) from rkbu a "
                . "inner join det_rkbu b on a.kd_rkbu=b.kd_rkbu "
                . "where a.kd_lks =$kdlks  and a.thn=$thn $filter";

        $sql = "select a.kd_rkbu,a.kd_skpd,a.kd_sub2,a.kd_lks,a.thn,a.nm_brg,a.spek,a.ket,a.kd_rek,b.jml as jmlrkb,b.satuan,b.hrg as hrgrkb,b.ttl as ttlrkb, "
                . " b.jml2 as jmldkb,b.hrg2 as hrgdkb,b.ttl2 as ttldkb from rkbu a "
                . " inner join det_rkbu b on a.kd_rkbu=b.kd_rkbu "
                . " where a.kd_lks =$kdlks and a.thn=$thn $filter";
       
        $count = \Yii::$app->db->createCommand($cnt)->queryScalar();
        
        $dataProvider=new SqlDataProvider(['sql'=>$sql, 
  //  'keyField' => $key,
    'totalCount'=>$count,
    'sort'=>[
      
        'attributes'=>[
           'nm_brg','jmlrkb','jmldkb','satuan','hrgrkb','hrgdkp',
            'ttlrkp','ttldkp','kd_rek','ket'
        ],
   ],
    'pagination'=>[
        'pageSize'=>100,
    ],
]);
return $dataProvider;

    }


    /**
     * Lists all Rkbu models.
     * @return mixed
     */
    
    
    public function actionIndex()
    {
        
        
        $dataProvider = $this->listrkbu();
               return $this->render('index', [
            'dataProvider' => $dataProvider,
			
        ]);
    }

 

    /**
     * Creates a new Rkbu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rkbu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rkbu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Rkbu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rkbu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rkbu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rkbu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }

    /**
     * Displays a single Rkbu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

}
