<?php

namespace app\controllers;

use Yii;
use app\models\RefBidang;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RefBidangController implements the CRUD actions for RefBidang model.
 */
class RefBidangController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

    /**
     * Lists all RefBidang models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => RefBidang::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single RefBidang model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new RefBidang model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new RefBidang();

        if ($model->load(Yii::$app->request->post()) && $model->validate() ) {
            //$model = new RefBidang();
                $kdbid=$model->kd_bid;
            $kdgol=$model->kd_gol;
            $model->kd_gol = $kdgol;
            $model->kd_bid = $kdgol . '.' . $kdbid;
         
            $model->save(false);
            return $this->redirect(['view', 'id' => $model->id]);
           // return var_dump($model->attributes);
            
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }
    public function actionGetBid($id){
        $countPosts = RefBidang::find()
                ->where(['kd_gol'=>$id])
         ->count();
        if ($countPosts>0){
            $bid = RefBidang::find()
                    ->select(['kd_bid','kd_gol'])
                    ->where(['kd_gol'=>$id])
                    ->all();
            foreach ($bid as $key => $value) {
                $count = substr($value['kd_bid'], -2);
            }
            
        }else{
            $count='0';
        }
        
        return \Yii::$app->MyComponent->tambah_nol($count+1,2);
    }
    /**
     * Updates an existing RefBidang model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing RefBidang model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the RefBidang model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return RefBidang the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = RefBidang::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
