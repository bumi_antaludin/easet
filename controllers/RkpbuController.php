<?php

namespace app\controllers;

use Yii;
use app\models\Rkpbu;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * RkpbuController implements the CRUD actions for Rkpbu model.
 */
class RkpbuController extends Controller
{
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
        ];
    }

 /*
  * List RKPBU
  */   
    public function listrkpbu($kdlks="' '",$thn="''",$jnsdata=1){
           
        switch ($jnsdata) {
            case 1: //seluruhan
                
                $filter =" ";

                break;
            case 2: //Barang Inventaris
                $filter = " AND kdbrg= ";
                break;
            case 3: //Barang Habis Pakai
                $filter = " AND left(kd_sub2,1)='5' ";
                break;
        }
              $cnt = "select count(*) from rkpbu a "
                . "inner join det_rkpbu b on a.kd_rkpbu=b.kd_rkpbu "
                . "where a.kd_lks=$kdlks and a.thn=$thn $filter";
        
        
        $sql = "select a.kd_rkpbu,a.kd_skpd,a.kd_sub2,a.kd_lks,a.thn,a.nm_brg,a.ket,"
                . " a.spek,a.kd_rek,a.sts_pelihara,b.jml,b.hrg,b.ttl from rkpbu a "
                . "inner join det_rkpbu b on a.kd_rkpbu=a.kd_rkpbu "
                . "where a.kd_lks=$kdlks and a.thn=$thn $filter";
     $count = Yii::$app->db->createCommand($cnt)->queryScalar();
     
     $dataProvider = new \yii\data\SqlDataProvider([
         'sql'=>$sql,
       'totalCount'=>$count,
         'sort'=>[
             'attributes'=>[
                 'nm_brg','sts_pelihara','kd_lks','kd_brg','jml','hrg','ttl','kd_rek','ket'
             ],
         ],
         'pagination'=>[
             'pageSize'=>100,
         ]
     ]);
     return $dataProvider;
    }
    /**
     * Lists all Rkpbu models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = $this->listrkpbu();
        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Rkpbu model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Rkpbu model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Rkpbu();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Updates an existing Rkpbu model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Deletes an existing Rkpbu model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Rkpbu model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Rkpbu the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Rkpbu::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested page does not exist.');
        }
    }
}
